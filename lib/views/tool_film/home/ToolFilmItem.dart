import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/ToolFilmViewModel.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class ToolFilmItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ToolFilmViewModel>(
        builder: (context, child, model) {
          model.fetchToolFilm();
      if (model.currentListToolFilm != null) {
        return new ListView.builder(
            itemCount: model.currentListToolFilm.length,
            itemBuilder: (BuildContext ctxt, int index) => Card(
                elevation: 8.0,
                margin:
                    new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                child: InkWell(
                  onTap: () => {
//                        Navigator.push(context, MaterialPageRoute(builder: (context) => ParentToolDetail(model, model.currentListTool[index].id)))
                  },
                  child: Container(
                    decoration:
                        BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                    child: ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        leading: Container(
                            padding: EdgeInsets.only(right: 12.0),
                            decoration: new BoxDecoration(
                                border: new Border(
                                    right: new BorderSide(
                                        width: 1.0, color: Colors.white24))),
                            child: Image.network(
                                model.currentListToolFilm[index].image)),
                        title: Text(
                          model.currentListToolFilm[index].toolName,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Column(
                          crossAxisAlignment:  CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                    "Số lượng: ${model.currentListToolFilm[index].quantity.toString()}",
                                    style: TextStyle(color: Colors.white)),
                                Expanded(child: Container()),
//                                Text("${model.currentListTool[index]
//                                    .status}",
//                                    style: model.currentListTool[index].status == "Available" ? TextStyle(color: Colors.green, fontWeight: FontWeight.bold) :
//                                    model.currentListTool[index].status == "Deleted" ? TextStyle(color: Colors.red, fontWeight: FontWeight.bold) :
//                                    model.currentListTool[index].status == "Out of stock" ? TextStyle(color: Colors.orange, fontWeight: FontWeight.bold) : Text("")
//                                ),
                              ],
                            ),
                            SizedBox(height: ScreenUtil.getInstance().setHeight(10)),
                            Text("${model.currentListToolFilm[index].borrowFrom}",
                                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                            Text("${model.currentListToolFilm[index].borrowTo}",
                                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))
                          ],
                        ),
                        trailing: RaisedButton.icon(onPressed: () async{
                          ProgressDialog pr = ProgressDialog(context);
                          pr.style(message: 'Chờ giây lát.',
                              borderRadius: 10.0,
                              backgroundColor: Colors.white,
                              progressWidget: CircularProgressIndicator(),
                              elevation: 10.0,
                              insetAnimCurve: Curves.easeInOut,
                              progress: 0.0,
                              maxProgress: 100.0,
                              progressTextStyle: TextStyle(
                                  color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
                              messageTextStyle: TextStyle(
                                  color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
                          await pr.show();
                          model.deleteTool(model.currentListToolFilm[index].id, context);
                          pr.hide();
                        }, icon: Icon(Icons.close, color: Colors.white,), label: Text("Trả lại",style: TextStyle(color: Colors.white),), color: Colors.red,)),
                  ),
                )));
      } else {
        return Container();
      }
    });
  }
}
