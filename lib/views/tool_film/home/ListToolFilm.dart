


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/ToolFilmViewModel.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/tool_film/add/ParentListToolToAdd.dart';
import 'package:journey/views/tool_film/home/ToolFilmItem.dart';
import 'package:scoped_model/scoped_model.dart';

class ListToolFilm extends StatelessWidget{


  ToolFilmViewModel model;


  ListToolFilm(this.model);

  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    model.fetchToolFilm();
    return ScopedModel<ToolFilmViewModel>(model: model, child: Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: ScreenUtil.getInstance().setHeight(150),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30.0),
            child: Container(
              child: Stack(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xB6B4B4).withOpacity(1),
                              offset: Offset(0.0, 8.0),
                              blurRadius: 20.0)
                        ],
                      ),
                      padding: EdgeInsets.only(left: 15),
                      height: ScreenUtil.getInstance().setHeight(350),
                      width: MediaQuery.of(context).size.width * 1,
                      child: Padding(
                        padding: EdgeInsets.only(top: 50.0),
                        child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Danh sách đạo cụ của: ${model.currentFilmName != null ? model.currentFilmName : ""}",
//                                    "asd",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: ScreenUtil.getInstance()
                                              .setSp(50),
                                          color: Colors.black),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            RaisedButton.icon(onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ParentListToolToAdd(ToolViewModel(), model.currentFilmId)));
                            }, icon: Icon(Icons.add), label: Text("Thêm đạo cụ"), color: Colors.green,)
                          ],
                        ),
                      )),
//                  FractionalTranslation(
//                      translation: Offset(3.0, 0.1),
//                      child: Container(
//                        width: ScreenUtil.getInstance().setWidth(250),
//                        height: ScreenUtil.getInstance().setHeight(240),
//                        decoration: BoxDecoration(
//                          shape: BoxShape.rectangle,
//                          image: new DecorationImage(
//                              image:
//                              new AssetImage("assets/tool.png")),
//                          border: new Border.all(
//                              color: Colors.black,
//                              width: 3.0,
//                              style: BorderStyle.solid),
//                        ),
//                      ))
                ],
              ),
            ),
          ),
          SizedBox(
            height: ScreenUtil.getInstance().setHeight(40),
          ),
          Flexible(child: ToolFilmItem())
        ],
      ),
    ));
  }

}