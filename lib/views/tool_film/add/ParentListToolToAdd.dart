



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/tool_film/add/ItemToolToAdd.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentListToolToAdd extends StatelessWidget{
  ToolViewModel model;
  String filmId;
  ParentListToolToAdd(this.model,this.filmId);

  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    model.filmId = filmId;
    model.fetchTool();
    return ScopedModel<ToolViewModel>(model: model, child: Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: ScreenUtil.getInstance().setHeight(150),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30.0),
            child: Container(
              child: Stack(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xB6B4B4).withOpacity(1),
                              offset: Offset(0.0, 8.0),
                              blurRadius: 20.0)
                        ],
                      ),
                      padding: EdgeInsets.only(left: 15),
                      height: ScreenUtil.getInstance().setHeight(300),
                      width: MediaQuery.of(context).size.width * 1,
                      child: Padding(
                        padding: EdgeInsets.only(top: 50.0),
                        child: Row(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Danh sách đạo cụ có sẵn",
//                                    "asd",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: ScreenUtil.getInstance()
                                          .setSp(50),
                                      color: Colors.black),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),
                  FractionalTranslation(
                      translation: Offset(3.0, 0.1),
                      child: Container(
                        width: ScreenUtil.getInstance().setWidth(250),
                        height: ScreenUtil.getInstance().setHeight(240),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: new DecorationImage(
                              image:
                              new AssetImage("assets/tool.png")),
                          border: new Border.all(
                              color: Colors.black,
                              width: 3.0,
                              style: BorderStyle.solid),
                        ),
                      ))
                ],
              ),
            ),
          ),
          SizedBox(
            height: ScreenUtil.getInstance().setHeight(40),
          ),
          Flexible(child: ItemToolToAdd())
        ],
      ),
    ));
  }
  
}