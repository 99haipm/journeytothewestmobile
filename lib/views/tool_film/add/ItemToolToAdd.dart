import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class ItemToolToAdd extends StatelessWidget {
  void _showDialog(BuildContext context, ToolViewModel model) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Thêm đạo cụ"),
          content: Container(
            width: ScreenUtil.getInstance().setHeight(100),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.event_note),
                  title: TextField(
                    controller: model.quantityController2,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(hintText: "Số lương"),
                    onChanged: (value) {
                      model.ChangeText("quantity", value);
                    },
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.date_range),
                  title: FlatButton(
                      onPressed: () {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: DateTime(2018, 3, 5),
                            maxTime: DateTime(2021, 6, 7),
                            theme: DatePickerTheme(
                                headerColor: Colors.orange,
                                backgroundColor: Colors.blue,
                                itemStyle: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                                doneStyle: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16)), onChanged: (date) {
                          print('change $date in time zone ' +
                              date.timeZoneOffset.inHours.toString());
                        }, onConfirm: (date) {
                          model.setTime("begin", date);
                          Navigator.of(context).pop();
                          _showDialog(context, model);
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                      },
                      child: Text(model.begin != null
                          ? model.begin.toIso8601String()
                          : "chọn ngày bắt đầu")),
                ),
                ListTile(
                  leading: Icon(Icons.date_range),
                  title: FlatButton(
                      onPressed: () {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: DateTime(2018, 3, 5),
                            maxTime: DateTime(2021, 6, 7),
                            theme: DatePickerTheme(
                                headerColor: Colors.orange,
                                backgroundColor: Colors.blue,
                                itemStyle: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                                doneStyle: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16)), onChanged: (date) {
                          print('change $date in time zone ' +
                              date.timeZoneOffset.inHours.toString());
                        }, onConfirm: (date) {
                          model.setTime("end", date);
                          _showDialog(context, model);
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                      },
                      child: Text(model.end != null
                          ? model.end.toIso8601String()
                          : "chọn ngày kết thúc")),
                ),
                RaisedButton(
                  child: new Text("Thêm"),
                  color: Colors.green,
                  onPressed: () async{
                    ProgressDialog pr = ProgressDialog(context);
                    pr.style(message: 'Chờ giây lát.',
                        borderRadius: 10.0,
                        backgroundColor: Colors.white,
                        progressWidget: CircularProgressIndicator(),
                        elevation: 10.0,
                        insetAnimCurve: Curves.easeInOut,
                        progress: 0.0,
                        maxProgress: 100.0,
                        progressTextStyle: TextStyle(
                            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
                        messageTextStyle: TextStyle(
                            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
                    await pr.show();
                    model.addToolToFilm(model.toolId, context);
                    pr.hide();
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            Row(
              children: <Widget>[
                RaisedButton(
                  child: new Text("Đóng"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ToolViewModel>(
        builder: (context, child, model) {
      if (model.currentListTool != null) {
        return new ListView.builder(
            itemCount: model.currentListTool.length,
            itemBuilder: (BuildContext ctxt, int index) => Card(
                elevation: 8.0,
                margin:
                    new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                child: InkWell(
                    onTap: () => {
//                            Navigator.push(context, MaterialPageRoute(builder: (context) => ParentToolDetail(model, model.currentListTool[index].id)))
                        },
                    child: Container(
                      decoration:
                          BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        leading: Container(
                            padding: EdgeInsets.only(right: 12.0),
                            decoration: new BoxDecoration(
                                border: new Border(
                                    right: new BorderSide(
                                        width: 1.0, color: Colors.white24))),
                            child: Image.network(
                                model.currentListTool[index].image)),
                        title: Text(
                          model.currentListTool[index].name,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Row(
                          children: <Widget>[
                            Text(
                                "Số lượng: ${model.currentListTool[index].quantity}",
                                style: TextStyle(color: Colors.white)),
                            Expanded(child: Container()),
                            Text("${model.currentListTool[index].status}",
                                style: model.currentListTool[index].status ==
                                        "Available"
                                    ? TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.bold)
                                    : model.currentListTool[index].status ==
                                            "Deleted"
                                        ? TextStyle(
                                            color: Colors.red,
                                            fontWeight: FontWeight.bold)
                                        : model.currentListTool[index].status ==
                                                "Out of stock"
                                            ? TextStyle(
                                                color: Colors.orange,
                                                fontWeight: FontWeight.bold)
                                            : Text("")),
                          ],
                        ),
                        trailing:
                            model.currentListTool[index].status == "Available"
                                ? RaisedButton(
                                    onPressed: () {
                                      model.maxQuantity =
                                          model.currentListTool[index].quantity;
                                      model.toolId =
                                          model.currentListTool[index].id;
                                      _showDialog(context, model);
                                    },
                                    color: Colors.green,
                                    child: Text("Thêm"),
                                  )
                                : RaisedButton.icon(
                                    onPressed: () {},
                                    color: Colors.red,
                                    icon: Icon(Icons.warning),
                                    label: Text("Hết"),
                                  ),
                      ),
                    ))));
      } else {
        return Container();
      }
    });
  }
}
