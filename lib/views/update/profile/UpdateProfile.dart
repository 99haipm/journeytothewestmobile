import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:journey/view_models/UpdateProfileViewModel.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class UpdateProfile extends StatelessWidget {
  String id;


  UpdateProfile(this.id);

  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);

    return ScopedModelDescendant<UpdateProfileViewModel>(
        builder: (context, child, model) {
          if(model.status){
            return SuccessDialog(title: "Cập nhập thành công", description: "", buttonText: "Go To home page",isLoginAgain: true,);
          }
          else{
            return Scaffold(
                body: Padding(
                    padding: EdgeInsets.only(top: 150.0, left: 25.0),
                    child: Container(
                      padding: new EdgeInsets.only(left: 5.0, right: 10.0, top: 50),
                      width: ScreenUtil.getInstance().setWidth(950),
                      height: ScreenUtil.getInstance().setHeight(1000),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                offset: Offset(0.0, 15.0),
                                blurRadius: 15.0),
                            BoxShadow(
                                color: Colors.black12,
                                offset: Offset(0.0, -10.0),
                                blurRadius: 10.0),
                          ]),
                      child: Padding(
                        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Text(
                                "Set Up Your Account",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: ScreenUtil.getInstance().setSp(50)),
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.person),
                              title: TextField(
                                decoration: InputDecoration(hintText: "Name"),
                                onChanged: (value) {
                                  model.ChangeText("name", value);
                                },
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.phone),
                              title: TextField(
                                decoration: InputDecoration(hintText: "Phone"),
                                onChanged: (value) {
                                  model.ChangeText("phone", value);
                                },
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.mail),
                              title: TextField(
                                decoration: InputDecoration(hintText: "Email"),
                                onChanged: (value) {
                                  model.ChangeText("email", value);
                                },
                              ),
                            ),
                            Flexible(
                              child: ListTile(
                                leading: FloatingActionButton(
                                  onPressed: () {
                                    model.PickImage();
                                  },
                                  tooltip: 'Pick Image',
                                  child: Icon(Icons.add_a_photo),
                                ),
                                title: Center(
                                  child: model.avatar == null
                                      ? Text('No image selected.')
                                      : Image.file(model.avatar),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(50),
                            ),
                            Center(
                                child: AnimatedContainer(
                                  height: ScreenUtil.getInstance().setHeight(100),
                                  width: ScreenUtil.getInstance().setWidth(400),
                                  duration: Duration(seconds: 2),
                                  curve: Curves.bounceIn,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          colors: [Colors.red, Colors.yellow]),
                                      borderRadius: BorderRadius.circular(6.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0xFF6078ea).withOpacity(.3),
                                            offset: Offset(0.0, 8.0),
                                            blurRadius: 8.0)
                                      ],
                                    ),
                                    child: InkWell(
                                      onTap: () async{

                                        model.UpdateProfile(id,context);
                                      },
                                      child: Center(
                                        child: Text(
                                          "Update",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ),
                                ))
                          ],
                        ),
                      ),
                    )));
          }
    });
  }
}
