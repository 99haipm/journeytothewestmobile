



import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/UpdateProfileViewModel.dart';
import 'package:journey/views/update/profile/UpdateProfile.dart';
import 'package:scoped_model/scoped_model.dart';

class UpdateProfileParent extends StatelessWidget{

  UpdateProfileViewModel model;
  String id;


  UpdateProfileParent(this.model, this.id);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<UpdateProfileViewModel>(model: model, child: UpdateProfile(id));
  }

}