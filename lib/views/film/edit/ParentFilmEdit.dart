


import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/FilmViewModel.dart';
import 'package:journey/views/film/edit/FilmEdit.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentFilmEdit extends StatelessWidget{
  FilmViewModel model;

  ParentFilmEdit(this.model);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<FilmViewModel>(model: model, child: FilmEdit());
  }

}