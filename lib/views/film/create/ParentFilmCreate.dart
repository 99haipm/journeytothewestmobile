


import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/FilmViewModel.dart';
import 'package:journey/views/film/create/FilmCreate.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentFilmCreate extends StatelessWidget{
  FilmViewModel model;

  ParentFilmCreate(this.model);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<FilmViewModel>(model: model, child: FilmCreate());
  }

}