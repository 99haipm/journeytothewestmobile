


import 'package:flutter/material.dart';
import 'package:journey/view_models/FilmViewModel.dart';
import 'package:journey/views/film/detail/FilmDetail.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentFilmDetail extends StatelessWidget{

  FilmViewModel model;
  String id;

  ParentFilmDetail(this.model,this.id);

  @override
  Widget build(BuildContext context) {
    model.getById(id);
    return ScopedModel<FilmViewModel>(model: model, child: FilmDetail());
  }

}