import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:journey/view_models/FilmViewModel.dart';
import 'package:journey/view_models/ToolFilmViewModel.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:journey/views/film/create/ParentFilmCreate.dart';
import 'package:journey/views/film/edit/ParentFilmEdit.dart';
import 'package:journey/views/film/home/ParentListFilm.dart';
import 'package:journey/views/tool_film/home/ListToolFilm.dart';
import 'package:journey/views/user_film/home/ParentListUserFilm.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class FilmDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    return WillPopScope(onWillPop: () {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ParentListFilm(FilmViewModel())));
    }, child:
        ScopedModelDescendant<FilmViewModel>(builder: (context, child, model) {
      if (model.currentFilm != null) {
        model.getById(model.currentFilm.id);
      }
      return Scaffold(
          body: Padding(
              padding: EdgeInsets.only(top: 50.0),
              child: Container(
                child: Center(
                  child: Container(
                    margin: new EdgeInsets.only(left: 10.0, right: 10.0),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xB6B4B4).withOpacity(1),
                            offset: Offset(0.0, 8.0),
                            blurRadius: 20.0)
                      ],
                    ),
                    child: new SizedBox(
                        height: 500,
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                leading: new Icon(
                                  Icons.account_box,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Tên",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentFilm != null
                                    ? model.currentFilm.name
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.edit_location,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Địa điểm",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentFilm != null
                                    ? model.currentFilm.location
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.date_range,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Ngày bắt đầu",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentFilm != null
                                    ? model.currentFilm.begin
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                  leading: new Icon(
                                    Icons.date_range,
                                    color: Colors.blue,
                                    size: 26.0,
                                  ),
                                  title: new Text(
                                    "Ngày kết thúc",
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w400),
                                  ),
                                  subtitle: new Text(model.currentFilm != null
                                      ? model.currentFilm.end
                                      : "")),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                  leading: new Icon(
                                    Icons.work,
                                    color: Colors.blue,
                                    size: 26.0,
                                  ),
                                  title: new Text(
                                    "Status",
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w400),
                                  ),
                                  subtitle: new Text(model.currentFilm != null
                                      ? model.currentFilm.status
                                      : "")),
                              model.currentFilm != null
                                  ? model.currentFilm.status != "Delete"
                                      ? Row(
                                          children: <Widget>[
                                            AnimatedContainer(
                                              height: ScreenUtil.getInstance()
                                                  .setHeight(100),
                                              width: ScreenUtil.getInstance()
                                                  .setWidth(200),
                                              duration: Duration(seconds: 2),
                                              curve: Curves.bounceIn,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: Colors.red,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          6.0),
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Color(0xFF6078ea)
                                                            .withOpacity(.3),
                                                        offset:
                                                            Offset(0.0, 8.0),
                                                        blurRadius: 8.0)
                                                  ],
                                                ),
                                                child: InkWell(
                                                  onTap: () async{
                                                    ProgressDialog pr = ProgressDialog(context);
                                                    pr.style(message: 'Chờ giây lát.',
                                                        borderRadius: 10.0,
                                                        backgroundColor: Colors.white,
                                                        progressWidget: CircularProgressIndicator(),
                                                        elevation: 10.0,
                                                        insetAnimCurve: Curves.easeInOut,
                                                        progress: 0.0,
                                                        maxProgress: 100.0,
                                                        progressTextStyle: TextStyle(
                                                            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
                                                        messageTextStyle: TextStyle(
                                                            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
                                                    await pr.show();
                                                    var check = model.delete(
                                                        model.currentFilm.id,
                                                        context);
                                                    pr.hide();
                                                  },
                                                  child: Center(
                                                    child: Text(
                                                      "Xóa",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize:  ScreenUtil.getInstance().setSp(45),
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: ScreenUtil.getInstance().setWidth(30),),
                                            AnimatedContainer(
                                              height: ScreenUtil.getInstance()
                                                  .setHeight(100),
                                              width: ScreenUtil.getInstance()
                                                  .setWidth(250),
                                              duration: Duration(seconds: 2),
                                              curve: Curves.bounceIn,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                 color: Colors.blue,
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      6.0),
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Color(0xFF6078ea)
                                                            .withOpacity(.3),
                                                        offset:
                                                        Offset(0.0, 8.0),
                                                        blurRadius: 8.0)
                                                  ],
                                                ),
                                                child: InkWell(
                                                  onTap: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ParentFilmEdit(model)));
                                                  },
                                                  child: Center(
                                                    child: Text(
                                                      "Chỉnh sửa",
                                                      style: TextStyle(
                                                          fontWeight:
                                                          FontWeight.bold,
                                                          fontSize: ScreenUtil.getInstance().setSp(45),
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: ScreenUtil.getInstance().setWidth(25),),
                                            AnimatedContainer(
                                              height: ScreenUtil.getInstance()
                                                  .setHeight(100),
                                              width: ScreenUtil.getInstance()
                                                  .setWidth(200),
                                              duration: Duration(seconds: 2),
                                              curve: Curves.bounceIn,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: Colors.green,
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      6.0),
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Color(0xFF6078ea)
                                                            .withOpacity(.3),
                                                        offset:
                                                        Offset(0.0, 8.0),
                                                        blurRadius: 8.0)
                                                  ],
                                                ),
                                                child: InkWell(
                                                  onTap: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ListToolFilm(ToolFilmViewModel(model.currentFilm.id,model.currentFilm.name))));
                                                  },
                                                  child: Center(
                                                    child: Text(
                                                      "Đạo Cụ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                          FontWeight.bold,
                                                          fontSize:  ScreenUtil.getInstance().setSp(45),
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(width: ScreenUtil.getInstance().setWidth(25),),
                                            AnimatedContainer(
                                              height: ScreenUtil.getInstance()
                                                  .setHeight(100),
                                              width: ScreenUtil.getInstance()
                                                  .setWidth(250),
                                              duration: Duration(seconds: 2),
                                              curve: Curves.bounceIn,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: Colors.orange,
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      6.0),
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Color(0xFF6078ea)
                                                            .withOpacity(.3),
                                                        offset:
                                                        Offset(0.0, 8.0),
                                                        blurRadius: 8.0)
                                                  ],
                                                ),
                                                child: InkWell(
                                                  onTap: () {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                ParentListUserFilm(CharacterViewModel(model.currentFilm.id),model.currentFilm.name)));
                                                  },
                                                  child: Center(
                                                    child: Text(
                                                      "Diễn Viên",
                                                      style: TextStyle(
                                                          fontWeight:
                                                          FontWeight.bold,
                                                          fontSize:  ScreenUtil.getInstance().setSp(45),
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        )
                                      : AnimatedContainer(
                                          height: ScreenUtil.getInstance()
                                              .setHeight(100),
                                          width: ScreenUtil.getInstance()
                                              .setWidth(400),
                                          duration: Duration(seconds: 2),
                                          curve: Curves.bounceIn,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              gradient: LinearGradient(colors: [
                                                Colors.red,
                                                Colors.yellow
                                              ]),
                                              borderRadius:
                                                  BorderRadius.circular(6.0),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Color(0xFF6078ea)
                                                        .withOpacity(.3),
                                                    offset: Offset(0.0, 8.0),
                                                    blurRadius: 8.0)
                                              ],
                                            ),
                                            child: InkWell(
                                              onTap: () {},
                                              child: Center(
                                                child: Text(
                                                  "Đã xóa",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize:  ScreenUtil.getInstance().setSp(45),
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                  : Expanded(child: Container())
                            ],
                          ),
                        )),
                  ),
                ),
              )));
    }));
  }
}
