import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/view_models/FilmViewModel.dart';
import 'package:journey/views/film/detail/ParentFilmDetail.dart';
import 'package:scoped_model/scoped_model.dart';

class FilmItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<FilmViewModel>(
        builder: (context, child, model) {
          if(model.afterFinish){
            model.fetchFilm();
          }
      return new ListView.builder(
          itemCount: model.tempFilm.length,
          itemBuilder: (BuildContext ctxt, int index) => Card(
              elevation: 8.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: InkWell(
                onTap: () => {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ParentFilmDetail(FilmViewModel(),model.tempFilm[index].id)))
                },
                child: Container(
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                  child: ListTile(
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      title: Text(
                        model.tempFilm[index].name,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                  "From ${model.tempFilm[index].begin}",
                                  style: TextStyle(color: Colors.white)),
                              Text(
                                  "To ${model.tempFilm[index].end}",
                                  style: TextStyle(color: Colors.white)),
                              model.tempFilm[index].status == "New" ?
                                Text(
                                    "${model.tempFilm[index].status}",
                                    style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold))
                                  : model.tempFilm[index].status == "Process"?
                              Text(
                                  "${model.tempFilm[index].status}",
                                  style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold))
                                  : model.tempFilm[index].status == "Finished"?
                              Text(
                                  "${model.tempFilm[index].status}",
                                  style: TextStyle(color: Colors.yellow,fontWeight: FontWeight.bold))
                                  :model.tempFilm[index].status == "Delete"?
                              Text(
                                  "${model.tempFilm[index].status}",
                                  style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold)) : null
                            ],
                          ),

                        ],
                      ),
                      trailing: model.tempFilm[index].status != "Delete" && model.tempFilm[index].status != "Finished" ? RaisedButton(onPressed: (){
                        model.finishFilm(model.tempFilm[index].id, context);
                      }, child: Text("Hoàn thành", style: TextStyle(color: Colors.black)),color: Colors.green,): Icon(Icons.keyboard_arrow_right,
                          color: Colors.white, size: 30.0)),
                ),
              )));
    });
  }
}
