


import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/DetailViewModel.dart';
import 'package:journey/views/manage/DetailActor.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentDetailActor extends StatelessWidget{
  DetailViewModel model;

  ParentDetailActor({this.model});

  @override
  Widget build(BuildContext context) {
    model.LoadActorDetail();
    return ScopedModel<DetailViewModel>(model: model, child: ActorDetail());
  }

}