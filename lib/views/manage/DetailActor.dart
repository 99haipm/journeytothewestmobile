import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart';
import 'package:flutter/material.dart';
import 'package:journey/view_models/DetailViewModel.dart';
import 'package:journey/view_models/ManageActorViewModel.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:journey/views/manage/MyListUser.dart';
import 'package:scoped_model/scoped_model.dart';

class ActorDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);

    return WillPopScope(onWillPop: () {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MyListUser(
                model: ManageActorViewModel(),
              )));
    }, child: ScopedModelDescendant<DetailViewModel>(
        builder: (context, child, model) {
      model.LoadActorDetail();
      return Scaffold(
          body: Padding(
              padding: EdgeInsets.only(top: 50.0),
              child: Container(
                child: Center(
                  child: Container(
                    margin: new EdgeInsets.only(left: 10.0, right: 10.0),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xB6B4B4).withOpacity(1),
                            offset: Offset(0.0, 8.0),
                            blurRadius: 20.0)
                      ],
                    ),
                    child: new SizedBox(
                        height: ScreenUtil.getInstance().setHeight(1350),
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: ListTile(
                                    title: Container(
                                  width: ScreenUtil.getInstance().setWidth(100),
                                  height:
                                      ScreenUtil.getInstance().setHeight(200),
                                  decoration: BoxDecoration(
                                    image: new DecorationImage(
                                        image: new NetworkImage(model
                                                    .currentActor !=
                                                null
                                            ? model.currentActor.avatar
                                            : "https://i.picsum.photos/id/9/250/250.jpg?hmac=tqDH5wEWHDN76mBIWEPzg1in6egMl49qZeguSaH9_VI")),
                                  ),
                                )),
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(64, 75, 96, .9)),
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.account_box,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Tên",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentActor != null
                                    ? model.currentActor.name
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.mail,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Email",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentActor != null
                                    ? model.currentActor.email
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.phone,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Số điện thoại",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentActor != null
                                    ? model.currentActor.phone
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                  leading: new Icon(
                                    Icons.work,
                                    color: Colors.blue,
                                    size: 26.0,
                                  ),
                                  title: new Text(
                                    "Status",
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w400),
                                  ),
                                  subtitle: model.currentActor != null
                                      ? model.currentActor.status
                                          ? new Icon(
                                              Icons.check_circle,
                                              color: Colors.lightGreen,
                                              size: 50,
                                            )
                                          : new Icon(
                                              Icons.close,
                                              color: Colors.red,
                                              size: 50,
                                            )
                                      : null),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              model.currentActor != null &&
                                      model.currentActor.status
                                  ? AnimatedContainer(
                                      height: ScreenUtil.getInstance()
                                          .setHeight(100),
                                      width: ScreenUtil.getInstance()
                                          .setWidth(400),
                                      duration: Duration(seconds: 2),
                                      curve: Curves.bounceIn,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(colors: [
                                            Colors.red,
                                            Colors.yellow
                                          ]),
                                          borderRadius:
                                              BorderRadius.circular(6.0),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0xFF6078ea)
                                                    .withOpacity(.3),
                                                offset: Offset(0.0, 8.0),
                                                blurRadius: 8.0)
                                          ],
                                        ),
                                        child: InkWell(
                                          onTap: () {
                                            var check = model.DeleteActor(
                                                model.currentActor.id);
                                            check.then((value) {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SuccessDialog(
                                                            title:
                                                                "Ban thành công",
                                                            description: "",
                                                            buttonText: "Ok",
                                                            isLoginAgain: false,
                                                          )));
                                            });
                                          },
                                          child: Center(
                                            child: Text(
                                              "Band",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  : AnimatedContainer(
                                      height: ScreenUtil.getInstance()
                                          .setHeight(100),
                                      width: ScreenUtil.getInstance()
                                          .setWidth(400),
                                      duration: Duration(seconds: 2),
                                      curve: Curves.bounceIn,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(colors: [
                                            Colors.green,
                                            Colors.yellow
                                          ]),
                                          borderRadius:
                                              BorderRadius.circular(6.0),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0xFF6078ea)
                                                    .withOpacity(.3),
                                                offset: Offset(0.0, 8.0),
                                                blurRadius: 8.0)
                                          ],
                                        ),
                                        child: InkWell(
                                          onTap: () {
                                            var check = model.ActiveActor(
                                                model.currentActor.id);
                                            check.then((value) {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          SuccessDialog(
                                                            title:
                                                                "Active thành công",
                                                            description: "",
                                                            buttonText: "Ok",
                                                            isLoginAgain: false,
                                                          )));
                                            });
                                          },
                                          child: Center(
                                            child: Text(
                                              "Active",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                            ],
                          ),
                        )),
                  ),
                ),
              )));
    }));
  }
}
