import 'package:flutter/material.dart';
import 'package:journey/models/Actor.dart';
import 'package:journey/models/User.dart';
import 'package:journey/view_models/DetailViewModel.dart';
import 'package:journey/view_models/ManageActorViewModel.dart';
import 'package:journey/views/manage/ParentDetailActor.dart';
import 'package:scoped_model/scoped_model.dart';

import 'DetailActor.dart';

class UserItems extends StatelessWidget {


  @override
  StatelessElement createElement() {
    return super.createElement();
  }




  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ManageActorViewModel>(
        builder: (context, child, model) {
          return new ListView.builder(
              itemCount: model.temptActor.length,
              itemBuilder: (BuildContext ctxt, int index) =>
                  Card(
                      elevation: 8.0,
                      margin: new EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 6.0),
                      child:
                      InkWell(
                        onTap: () => {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ParentDetailActor(model: DetailViewModel(id: model.temptActor[index].id),)))
                      },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(64, 75, 96, .9)),
                          child: ListTile(
                              contentPadding:
                              EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: new BoxDecoration(
                                      border: new Border(
                                          right: new BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Image.network(
                                      model.temptActor[index].avatar)),
                              title: Text(
                                model.temptActor[index].name,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Row(
                                children: <Widget>[
                                  Text("Email: ${model.temptActor[index]
                                      .email}",
                                      style: TextStyle(color: Colors.white)),
                                  Expanded(child: Container()),
                                  model.temptActor[index].status ?
                                  Icon(Icons.check_circle, color: Colors.green,)
                                      : Icon(Icons.close,  color: Colors.red)
                                ],
                              ),
                              trailing: Icon(Icons.keyboard_arrow_right,
                                  color: Colors.white, size: 30.0)),
                        ),
                      )

                  )
          );
        }
    );
  }
}
