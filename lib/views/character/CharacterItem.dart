import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';

class CharacterItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CharacterViewModel>(
        builder: (context, child, model) {
      model.fetchCharacterOfActor();
      return new ListView.builder(
          itemCount: model.characterOfActors.length,
          itemBuilder: (BuildContext ctxt, int index) => Card(
              elevation: 8.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: InkWell(
                onTap: () => {
//                      Navigator.push(context, MaterialPageRoute(builder: (context) => ParentFilmDetail(FilmViewModel(),model.tempFilm[index].id)))
                },
                child: Container(
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                  child: ListTile(
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      title: Text(
                        "Vai: " + model.characterOfActors[index].characterName,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                  "Tên cảnh: ${model.characterOfActors[index].filmName}",
                                  style: TextStyle(color: Colors.white)),
                            ],
                          ),
                        ],
                      ),
                      trailing: RaisedButton(
                        onPressed: ()async {
                          if(await canLaunch(model.characterOfActors[index].script)){
                            await launch(model.characterOfActors[index].script);
                          }
                        },
                        child: Text(
                          "Xem kịch bản",
                          style: TextStyle(color: Colors.black),
                        ),
                        color: Colors.green,
                      )),
                ),
              )));
    });
  }
}
