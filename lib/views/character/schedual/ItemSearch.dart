

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:scoped_model/scoped_model.dart';

class ItemSearch extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CharacterViewModel>(builder: (context,child,model){
      return  Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Color(0xB6B4B4).withOpacity(1),
                  offset: Offset(0.0, 8.0),
                  blurRadius: 20.0)
            ],
          ),
          padding: EdgeInsets.only(left: 15),
          height: ScreenUtil.getInstance().setHeight(350),
          width: MediaQuery.of(context).size.width * 0.7,
          child: Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: DateTime(2018, 3, 5),
                            maxTime: DateTime(2021, 6, 7),
                            theme: DatePickerTheme(
                                headerColor: Colors.orange,
                                backgroundColor: Colors.blue,
                                itemStyle: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                                doneStyle: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16)), onChanged: (date) {
                              print('change $date in time zone ' +
                                  date.timeZoneOffset.inHours.toString());
                            }, onConfirm: (date) {
                              model.setTime("begin", date);
//                              _showDialog(context, model);
                            },
                            currentTime: DateTime.now(),
                            locale: LocaleType.en);
                      },
                      child: Text("Ngày bắt đầu"),
                    ),
                    RaisedButton(
                      onPressed: () {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: DateTime(2018, 3, 5),
                            maxTime: DateTime(2021, 6, 7),
                            theme: DatePickerTheme(
                                headerColor: Colors.orange,
                                backgroundColor: Colors.blue,
                                itemStyle: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                                doneStyle: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16)), onChanged: (date) {
                              print('change $date in time zone ' +
                                  date.timeZoneOffset.inHours.toString());
                            }, onConfirm: (date) {
                              model.setTime("end", date);
//                              _showDialog(context, model);
                            },
                            currentTime: DateTime.now(),
                            locale: LocaleType.en);
                      },
                      child: Text("Ngày kết thúc"),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("${model.begin != null ? model.begin.day.toString()+ "/" + model.begin.month.toString() +"/" + model.begin.year.toString() : ""}"),
                    Text("---"),
                    Text("${model.end != null ? model.end.day.toString()+ "/" + model.end.month.toString() +"/" + model.end.year.toString() : ""}"),
                  ],
                ),
                RaisedButton(
                  child: Text("Tìm"),
                  onPressed: (){
                    model.clickToSearch();
                  },
                )
              ],
            ),
          ));
    });
  }

}