


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:scoped_model/scoped_model.dart';

class SchedualItem extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CharacterViewModel>(
        builder: (context, child, model) {
          return new ListView.builder(
              itemCount: model.currentListFilm.length,
              itemBuilder: (BuildContext ctxt, int index) => Card(
                  elevation: 8.0,
                  margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                  child: InkWell(
                    onTap: () => {
//                      Navigator.push(context, MaterialPageRoute(builder: (context) => ParentFilmDetail(FilmViewModel(),model.tempFilm[index].id)))
                    },
                    child: Container(
                      decoration:
                      BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                      child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 10.0),
                          title: Text(
                            model.currentListFilm[index].name,
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.bold),
                          ),
                          subtitle: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                      "From ${model.currentListFilm[index].begin}",
                                      style: TextStyle(color: Colors.white)),
                                  Text(
                                      "To ${model.currentListFilm[index].end}",
                                      style: TextStyle(color: Colors.white)),
                                  model.currentListFilm[index].status == "New" ?
                                  Text(
                                      "${model.currentListFilm[index].status}",
                                      style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold))
                                      : model.currentListFilm[index].status == "Process"?
                                  Text(
                                      "${model.currentListFilm[index].status}",
                                      style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold))
                                      : model.currentListFilm[index].status == "Finished"?
                                  Text(
                                      "${model.currentListFilm[index].status}",
                                      style: TextStyle(color: Colors.yellow,fontWeight: FontWeight.bold))
                                      :model.currentListFilm[index].status == "Delete"?
                                  Text(
                                      "${model.currentListFilm[index].status}",
                                      style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold)) : null
                                ],
                              ),

                            ],
                          ),
                          trailing: Icon(Icons.keyboard_arrow_right,
                              color: Colors.white, size: 30.0)),
                    ),
                  )));
        });
  }

}