





import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/UpdateProfileViewModel.dart';
import 'package:journey/views/character/detail/DetailActor.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentDetailActorUpdate extends StatelessWidget{

  UpdateProfileViewModel model;
  String id;


  ParentDetailActorUpdate(this.model, this.id);

  @override
  Widget build(BuildContext context) {
    model.id = id;
    model.fetchActor();
    return ScopedModel<UpdateProfileViewModel>(model: model, child: DetailActor());
  }

}