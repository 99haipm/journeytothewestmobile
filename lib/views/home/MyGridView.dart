import "package:flutter/material.dart";
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/HomeViewModel.dart';
import 'package:scoped_model/scoped_model.dart';

class MyGridView extends StatelessWidget {

  List<Items> mylist;

  MyGridView({this.mylist});


  @override
  Widget build(BuildContext context) {

    var color = Colors.white;
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    return ScopedModelDescendant<HomeViewModel>(
        builder: (context, child, model) {
      return Flexible(
          child: GridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 18,
              childAspectRatio: 1.0,
              padding: EdgeInsets.only(left: 16, right: 16),
              mainAxisSpacing: 16,
              children: mylist.map((data) {
                return InkWell(
                  onTap: () => model.changeScreen(data.screen, context),
                  child: Container(
                    decoration: BoxDecoration(
                      color: color, borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xFF6078ea).withOpacity(.3),
                            offset: Offset(0.0, 8.0),
                            blurRadius: 8.0)
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(data.img,
                            width: ScreenUtil.getInstance().setWidth(150)),
                        SizedBox(
                          height: ScreenUtil.getInstance().setHeight(14),
                        ),
                        Text(
                          data.title,
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: ScreenUtil.getInstance().setSp(50),
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: ScreenUtil.getInstance().setHeight(14),
                        ),
                      ],
                    ),
                  )
                );}).toList()));
    });
  }
}


