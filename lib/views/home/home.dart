import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/HomeViewModel.dart';
import 'package:journey/view_models/LoginViewModel.dart';
import 'package:journey/views/dialog/BanDialog.dart';
import 'package:journey/views/home/MyGridView.dart';
import 'package:journey/views/login/login.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: must_be_immutable
class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    return ScopedModelDescendant<HomeViewModel>(builder: (context,child,model) {
      if(model.status){
        if(model.currentUser.status){
          return Scaffold(
            body: Column(
              children: <Widget>[
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(150),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 30.0),
                  child: Container(
                    child: Stack(
                      children: <Widget>[
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFF6078ea).withOpacity(.3),
                                    offset: Offset(0.0, 8.0),
                                    blurRadius: 50.0)
                              ],
                            ),
                            padding: EdgeInsets.only(left: 15),
                            height: ScreenUtil.getInstance().setHeight(300),
                            width: MediaQuery.of(context).size.width * 1,
                            child: Padding(
                              padding: EdgeInsets.only(top: 50.0),
                              child: Row(
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Tên: ${model.currentUser != null ? model.currentUser.username : ""}",
//                                    "asd",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:
                                            ScreenUtil.getInstance().setSp(50),
                                            color: Colors.black),
                                      ),
                                    ],
                                  ),
                                  Expanded(child: Container()),
                                  IconButton(
                                    icon: Image.asset("assets/notification.png"),
                                  ),
                                  IconButton(
                                    icon: Image.asset("assets/logout.png"),
                                    onPressed: () async{
                                      model.notificationManager.unSubribe("MyTopic_"+model.currentUser.id);
                                      SharedPreferences share = await SharedPreferences.getInstance();
                                      share.clear();
                                      Navigator.pop(context);
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage(LoginViewModel())));
                                    },
                                  )
                                ],
                              ),
                            )),
                        FractionalTranslation(
                            translation: Offset(1.8, -0.5),
                            child: Container(
                              width: ScreenUtil.getInstance().setWidth(250),
                              height: ScreenUtil.getInstance().setHeight(240),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                    image: model.currentUser != null && model.currentUser.role != "Admin" ? new NetworkImage(model.currentUser.avatar) : new AssetImage("assets/list-user.png")),
                                border: new Border.all(
                                    color: Colors.black,
                                    width: 3.0,
                                    style: BorderStyle.solid),
                              ),
                            ))
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(40),
                ),
                MyGridView(
                  mylist: model.myList,
                )
              ],
            ),
          );
        }else{
          return BanDialog();
        }
      }else{
        return Container();
      }
    });
  }
}
