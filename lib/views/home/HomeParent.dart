


import 'package:flutter/widgets.dart';
import 'package:journey/view_models/HomeViewModel.dart';
import 'package:journey/view_models/NotificationManager.dart';
import 'package:journey/views/home/home.dart';
import 'package:scoped_model/scoped_model.dart';

class HomeParent extends StatelessWidget{
  HomeViewModel model;
  HomeParent({this.model});
  @override
  Widget build(BuildContext context) {
    model.fetchCurrentUser(context);
//    NotificationManager notificationManager = NotificationManager();
//    if(model.currentUser != null && model.currentUser.role != "Admin"){
//      notificationManager.initFCM(context);
//    }
    return ScopedModel<HomeViewModel>(model: model, child: HomePage());
  }

}