



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/view_models/HisotryViewModel.dart';
import 'package:scoped_model/scoped_model.dart';

class HistoryUserItem extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<HistoryViewModel>(builder: (context,child,model) {
      model.fetchHistory();
      return new ListView.builder(
          itemCount: model.currentListHistory.length,
          itemBuilder: (BuildContext ctxt, int index) => Card(
              elevation: 8.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: InkWell(
                onTap: () => {
//                  Navigator.push(context, MaterialPageRoute(builder: (context) => ParentFilmDetail(FilmViewModel(),model.tempFilm[index].id)))
                },
                child: Container(
                  decoration:
                  BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                  child: ListTile(
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      title: Text(
                        "Người thay đổi: "+model.currentListHistory[index].by,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                  "Mô tả: ${model.currentListHistory[index].desc}",
                                  style: TextStyle(color: Colors.white)),
                              Text(
                                  "Thời điểm: ${model.currentListHistory[index].date}",
                                  style: TextStyle(color: Colors.white)),
                            ],
                          ),

                        ],
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right,
                          color: Colors.white, size: 30.0)
                  ),
                ),
              )));

    });
  }

}