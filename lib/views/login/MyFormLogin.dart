import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/LoginViewModel.dart';
import 'package:scoped_model/scoped_model.dart';

class FromCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<LoginViewModel>(
      builder: (context, child, model) {
        return new Container(
          width: double.infinity,
          padding: EdgeInsets.only(bottom: 1),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.0),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    offset: Offset(0.0, 15.0),
                    blurRadius: 15.0),
                BoxShadow(
                    color: Colors.black12,
                    offset: Offset(0.0, -10.0),
                    blurRadius: 10.0),
              ]),
          child: Padding(
            padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Login",
                        style: TextStyle(
                            fontSize: ScreenUtil.getInstance().setSp(60),
                            letterSpacing: .6,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                Text("Username",
                    style: TextStyle(
                      fontSize: ScreenUtil.getInstance().setSp(26),
                      letterSpacing: .6,
                      fontFamily: "Poppins-Medium",
                    )),
                TextField(
                  decoration: InputDecoration(
                      hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                  onChanged: (text){
                    model.onChange("username", text);
                  }
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(20),
                ),
                Text("PassWord",
                    style: TextStyle(
                        fontFamily: "Poppins-Medium",
                        fontSize: ScreenUtil.getInstance().setSp(26))),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                    onChanged: (text){
                      model.onChange("password", text);
                    }
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(35),
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(35),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AnimatedContainer(
                      height: ScreenUtil.getInstance().setHeight(100),
                      width: ScreenUtil.getInstance().setWidth(400),
                      duration: Duration(seconds: 2),
                      curve: Curves.bounceIn,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Colors.red, Colors.yellow]),
                          borderRadius: BorderRadius.circular(6.0),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFF6078ea).withOpacity(.3),
                                offset: Offset(0.0, 8.0),
                                blurRadius: 8.0)
                          ],
                        ),
                        child: InkWell(
                          onTap: () {
                            model.clickToLogin(context);
                          },
                          child: Center(
                            child: Text(
                              "Sign In",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: ScreenUtil.getInstance().setHeight(55),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
