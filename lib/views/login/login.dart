import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/LoginViewModel.dart';
import 'package:journey/views/login/MyFormLogin.dart';
import 'package:scoped_model/scoped_model.dart';

// ignore: must_be_immutable
class LoginPage extends StatelessWidget {
  LoginViewModel model;

  LoginPage(this.model);


  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    return ScopedModel<LoginViewModel>(
        model: model,
        child: Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomPadding: true,
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Image.asset("assets/image_login.png"),
                  ),
                  Expanded(child: Container()),
                  Image.asset("assets/image_login_2.png")
                ],
              ),
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 125.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: ScreenUtil.getInstance().setHeight(180),
                      ),
                      FromCard()
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
