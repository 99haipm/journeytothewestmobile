



import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/tool/create/ToolCreate.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentToolCreate extends StatelessWidget{

  ToolViewModel model;


  ParentToolCreate(this.model);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<ToolViewModel>(model: model, child: ToolCreate());
  }

}