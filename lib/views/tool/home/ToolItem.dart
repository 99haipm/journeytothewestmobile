



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/tool/detail/ParentToolDetail.dart';
import 'package:scoped_model/scoped_model.dart';

class  ToolItem extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<ToolViewModel>(
        builder: (context, child, model) {
          if(model.tempListTool != null){
            return new ListView.builder(
                itemCount: model.tempListTool.length,
                itemBuilder: (BuildContext ctxt, int index) =>
                    Card(
                        elevation: 8.0,
                        margin: new EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 6.0),
                        child:
                        InkWell(
                          onTap: () => {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ParentToolDetail(model, model.tempListTool[index].id)))
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(64, 75, 96, .9)),
                            child: ListTile(
                                contentPadding:
                                EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                    padding: EdgeInsets.only(right: 12.0),
                                    decoration: new BoxDecoration(
                                        border: new Border(
                                            right: new BorderSide(
                                                width: 1.0,
                                                color: Colors.white24))),
                                    child: Image.network(
                                        model.tempListTool[index].image)),
                                title: Text(
                                  model.tempListTool[index].name,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                                subtitle: Row(
                                  children: <Widget>[
                                    Text("Số lượng: ${model.tempListTool[index]
                                        .quantity}",
                                        style: TextStyle(color: Colors.white)),
                                    Expanded(child: Container()),
                                    Text("${model.tempListTool[index]
                                        .status}",
                                        style: model.tempListTool[index].status == "Available" ? TextStyle(color: Colors.green, fontWeight: FontWeight.bold) :
                                        model.tempListTool[index].status == "Deleted" ? TextStyle(color: Colors.red, fontWeight: FontWeight.bold) :
                                        model.tempListTool[index].status == "Out of stock" ? TextStyle(color: Colors.orange, fontWeight: FontWeight.bold) : Text("")
                                    ),
                                  ],
                                ),
                                trailing: Icon(Icons.keyboard_arrow_right,
                                    color: Colors.white, size: 30.0)),
                          ),
                        )

                    )
            );
          }
          else{
            return Container();
          }
        }
    );
  }

}