import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/tool/edit/ParentToolEdit.dart';
import 'package:journey/views/tool/home/ListTool.dart';
import 'package:scoped_model/scoped_model.dart';

class ToolDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);

    return WillPopScope(onWillPop: () {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => ListTool(ToolViewModel())));
    }, child:
        ScopedModelDescendant<ToolViewModel>(builder: (context, child, model) {
      model.fetchToolById(model.currentId);
      return Scaffold(
          body: Padding(
              padding: EdgeInsets.only(top: 50.0),
              child: Container(
                child: Center(
                  child: Container(
                    margin: new EdgeInsets.only(left: 10.0, right: 10.0),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xB6B4B4).withOpacity(1),
                            offset: Offset(0.0, 8.0),
                            blurRadius: 20.0)
                      ],
                    ),
                    child: new SizedBox(
                        height: 500,
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: ListTile(
                                    title: Container(
                                  width: ScreenUtil.getInstance().setWidth(100),
                                  height:
                                      ScreenUtil.getInstance().setHeight(200),
                                  decoration: BoxDecoration(
                                    image: new DecorationImage(
                                        image: new NetworkImage(model
                                                    .currentTool !=
                                                null
                                            ? model.currentTool.image
                                            : "https://i.picsum.photos/id/9/250/250.jpg?hmac=tqDH5wEWHDN76mBIWEPzg1in6egMl49qZeguSaH9_VI")),
                                  ),
                                )),
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(64, 75, 96, .9)),
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.account_box,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Tên",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentTool != null
                                    ? model.currentTool.name
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.description,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Mô tả",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentTool != null
                                    ? model.currentTool.description
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                leading: new Icon(
                                  Icons.phone,
                                  color: Colors.blue,
                                  size: 26.0,
                                ),
                                title: new Text(
                                  "Số lượng",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400),
                                ),
                                subtitle: new Text(model.currentTool != null
                                    ? model.currentTool.quantity.toString()
                                    : ""),
                              ),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                  leading: new Icon(
                                    Icons.work,
                                    color: Colors.blue,
                                    size: 26.0,
                                  ),
                                  title: new Text(
                                    "Trạng thái",
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w400),
                                  ),
                                  subtitle: model.currentTool != null
                                      ? model.currentTool.status == "Available"
                                          ? Text(
                                              model.currentTool.status,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.green),
                                            )
                                          : model.currentTool.status ==
                                                  "Out of stock"
                                              ? Text(
                                                  model.currentTool.status,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.orange),
                                                )
                                              : Text(
                                                  model.currentTool.status,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.red),
                                                )
                                      : Text("")),
                              new Divider(
                                color: Colors.blue,
                                indent: 0,
                                thickness: 1,
                              ),
                              Center(
                                  child: Row(
                                children: <Widget>[
                                  SizedBox(
                                    width:
                                        ScreenUtil.getInstance().setWidth(25),
                                  ),
                                  model.currentTool != null &&
                                          model.currentTool.status != "Deleted"
                                      ? AnimatedContainer(
                                          height: ScreenUtil.getInstance()
                                              .setHeight(100),
                                          width: ScreenUtil.getInstance()
                                              .setWidth(400),
                                          duration: Duration(seconds: 2),
                                          curve: Curves.bounceIn,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.red,
                                              borderRadius:
                                                  BorderRadius.circular(6.0),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Color(0xFF6078ea)
                                                        .withOpacity(.3),
                                                    offset: Offset(0.0, 8.0),
                                                    blurRadius: 8.0)
                                              ],
                                            ),
                                            child: InkWell(
                                              onTap: () {
                                                model.deleteTool(
                                                    model.currentTool.id,
                                                    context);
                                              },
                                              child: Center(
                                                child: Text(
                                                  "Xóa",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18,
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Text(""),
                                  SizedBox(
                                    width:
                                        ScreenUtil.getInstance().setWidth(25),
                                  ),
                                  AnimatedContainer(
                                    height:
                                        ScreenUtil.getInstance().setHeight(100),
                                    width:
                                        ScreenUtil.getInstance().setWidth(400),
                                    duration: Duration(seconds: 2),
                                    curve: Curves.bounceIn,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius:
                                            BorderRadius.circular(6.0),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Color(0xFF6078ea)
                                                  .withOpacity(.3),
                                              offset: Offset(0.0, 8.0),
                                              blurRadius: 8.0)
                                        ],
                                      ),
                                      child: InkWell(
                                        onTap: (){
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ParentToolEdit(
                                                          model
                                                        )));
                                        },
                                        child: Center(
                                          child: Text(
                                            "Chỉnh sửa",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),

//                                      : AnimatedContainer(
//                                    height: ScreenUtil.getInstance()
//                                        .setHeight(100),
//                                    width: ScreenUtil.getInstance()
//                                        .setWidth(400),
//                                    duration: Duration(seconds: 2),
//                                    curve: Curves.bounceIn,
//                                    child: Container(
//                                      decoration: BoxDecoration(
//                                        gradient: LinearGradient(colors: [
//                                          Colors.green,
//                                          Colors.yellow
//                                        ]),
//                                        borderRadius:
//                                        BorderRadius.circular(6.0),
//                                        boxShadow: [
//                                          BoxShadow(
//                                              color: Color(0xFF6078ea)
//                                                  .withOpacity(.3),
//                                              offset: Offset(0.0, 8.0),
//                                              blurRadius: 8.0)
//                                        ],
//                                      ),
//                                      child: InkWell(
//                                        onTap: () {
//                                          var check = model.ActiveActor(
//                                              model.currentActor.id);
//                                          check.then((value) {
//                                            Navigator.push(
//                                                context,
//                                                MaterialPageRoute(
//                                                    builder: (context) =>
//                                                        SuccessDialog(
//                                                          title:
//                                                          "Active thành công",
//                                                          description: "",
//                                                          buttonText: "Ok",
//                                                          isLoginAgain: false,
//                                                        )));
//                                          });
//                                        },
//                                        child: Center(
//                                          child: Text(
//                                            "Active",
//                                            style: TextStyle(
//                                                fontWeight: FontWeight.bold,
//                                                fontSize: 18,
//                                                color: Colors.white),
//                                          ),
//                                        ),
//                                      ),
//                                    ),
//                                  )
                            ],
                          ),
                        )),
                  ),
                ),
              )));
    }));
  }
}
