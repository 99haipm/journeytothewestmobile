



import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/tool/detail/ToolDetail.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentToolDetail extends StatelessWidget{
  ToolViewModel model;
  String id;

  ParentToolDetail(this.model,this.id);

  @override
  Widget build(BuildContext context) {
    model.fetchToolById(id);
    return ScopedModel<ToolViewModel>(model: model, child: ToolDetail());

}

}