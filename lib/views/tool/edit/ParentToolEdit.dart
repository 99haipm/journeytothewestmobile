



import 'package:flutter/cupertino.dart';
import 'package:journey/models/Tool.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/tool/edit/ToolEdit.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentToolEdit extends StatelessWidget{
  ToolViewModel model;

  ParentToolEdit(this.model);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<ToolViewModel>(model: model, child: ToolEdit());
  }

}