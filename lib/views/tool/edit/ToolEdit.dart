


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:scoped_model/scoped_model.dart';

class ToolEdit extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    return ScopedModelDescendant<ToolViewModel>(
        builder: (context, child, model) {
          if(!model.isChange){
            model.initText();
          }
          if (model.statusJob) {
            return SuccessDialog(title: "Chỉnh sửa dụng cụ Thành Công", description: "", buttonText: "Go To home page",isLoginAgain: false);
          } else {
            return Scaffold(
                body: Padding(
                    padding: EdgeInsets.only(top: 50.0, left: 25.0),
                    child: Container(
                      padding: new EdgeInsets.only(left: 5.0, right: 10.0, top: 50),
                      width: ScreenUtil.getInstance().setWidth(950),
                      height: ScreenUtil.getInstance().setHeight(1100),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                offset: Offset(0.0, 15.0),
                                blurRadius: 15.0),
                            BoxShadow(
                                color: Colors.black12,
                                offset: Offset(0.0, -10.0),
                                blurRadius: 10.0),
                          ]),
                      child: Padding(
                        padding:
                        EdgeInsets.only(left: 16.0, right: 16.0, top: 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Text(
                                "Chỉnh sửa dụng cụ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: ScreenUtil.getInstance().setSp(50)),
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.bookmark),
                              title: TextField(
                                controller: model.nameController,
                                decoration: InputDecoration(hintText: "Tên"),
                                onChanged: (value) {
                                  model.ChangeText("name", value);
                                },
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.description),
                              title: TextField(
                                controller: model.descController,
                                decoration: InputDecoration(hintText: "Mô tả"),
                                onChanged: (value) {
                                  model.ChangeText("desc", value);
                                },
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.location_on),
                              title: TextField(
                                controller: model.quantityController,
                                decoration: InputDecoration(hintText: "Số lượng"),
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  model.ChangeText("quantity", value);
                                },
                              ),
                            ),
                            Flexible(
                              child: ListTile(
                                leading: FloatingActionButton(
                                  onPressed: () {
                                    model.PickImage();
                                  },
                                  tooltip: 'Pick Image',
                                  child: Icon(Icons.add_a_photo),
                                ),
                                title: Center(
                                  child: model.image == null
                                      ? model.currentImage != null ? Image.network(model.currentImage) :
                                          Text('No image selected.')
                                      : Image.file(model.image),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: ScreenUtil.getInstance().setHeight(20),
                            ),
                            Center(
                                child: AnimatedContainer(
                                  height: ScreenUtil.getInstance().setHeight(100),
                                  width: ScreenUtil.getInstance().setWidth(400),
                                  duration: Duration(seconds: 2),
                                  curve: Curves.bounceIn,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(6.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0xFF6078ea).withOpacity(.3),
                                            offset: Offset(0.0, 8.0),
                                            blurRadius: 8.0)
                                      ],
                                    ),
                                    child: InkWell(
                                      onTap: () {
                                        model.updateTool(context);
                                      },
                                      child: Center(
                                        child: Text(
                                          "Chỉnh sửa",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ),
                                ))
                          ],
                        ),
                      ),
                    )));
          }
        });
  }
  
}