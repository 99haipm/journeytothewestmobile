import 'package:flutter/material.dart';
import 'package:journey/view_models/RegisterViewModel.dart';
import 'package:journey/views/register/UsernameRegister.dart';
import 'package:scoped_model/scoped_model.dart';



// ignore: must_be_immutable
class UserRegisterParent extends StatelessWidget{
  RegisterViewModel model;


  UserRegisterParent(this.model);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<RegisterViewModel>(model: model, child: UsernameFromRegister());
  }
  
}