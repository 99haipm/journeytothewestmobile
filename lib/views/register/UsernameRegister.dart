import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/models/User.dart';
import 'package:journey/view_models/RegisterViewModel.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class UsernameFromRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    return ScopedModelDescendant<RegisterViewModel>(
        builder: (context, child, model) {
      if (model.result) {
        return SuccessDialog(title: "Thêm Mới Thành Công", description: "", buttonText: "Go To home page");
      } else {
        return Scaffold(
            body: Padding(
                padding: EdgeInsets.only(top: 150.0, left: 25.0),
                child: Container(
                  padding: new EdgeInsets.only(left: 5.0, right: 10.0, top: 50),
                  width: ScreenUtil.getInstance().setWidth(950),
                  height: ScreenUtil.getInstance().setHeight(1000),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.0),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(0.0, 15.0),
                            blurRadius: 15.0),
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(0.0, -10.0),
                            blurRadius: 10.0),
                      ]),
                  child: Padding(
                    padding:
                        EdgeInsets.only(left: 16.0, right: 16.0, top: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Text(
                            "Create New Account",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: ScreenUtil.getInstance().setSp(50)),
                          ),
                        ),
                        ListTile(
                          leading: Icon(Icons.person),
                          title: TextField(
                            decoration: InputDecoration(hintText: "Username"),
                            onChanged: (value) {
                              model.changeText("username", value);
                            },
                          ),
                        ),
                        ListTile(
                          leading: Icon(Icons.lock),
                          title: TextField(
                            obscureText: true,
                            decoration: InputDecoration(hintText: "Password"),
                            onChanged: (value) {
                              model.changeText("password", value);
                            },
                          ),
                        ),
                        ListTile(
                          leading: Icon(Icons.lock),
                          title: TextField(
                            obscureText: true,
                            decoration:
                                InputDecoration(hintText: "Confirm Password"),
                            onChanged: (value) {
                              model.changeText("confirm", value);
                            },
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil.getInstance().setHeight(50),
                        ),
                        Center(
                            child: AnimatedContainer(
                          height: ScreenUtil.getInstance().setHeight(100),
                          width: ScreenUtil.getInstance().setWidth(400),
                          duration: Duration(seconds: 2),
                          curve: Curves.bounceIn,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [Colors.red, Colors.yellow]),
                              borderRadius: BorderRadius.circular(6.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFF6078ea).withOpacity(.3),
                                    offset: Offset(0.0, 8.0),
                                    blurRadius: 8.0)
                              ],
                            ),
                            child: InkWell(
                              onTap: () async{
                                ProgressDialog pr = ProgressDialog(context);
                                pr.style(message: 'Chờ giây lát.',
                                    borderRadius: 10.0,
                                    backgroundColor: Colors.white,
                                    progressWidget: CircularProgressIndicator(),
                                    elevation: 10.0,
                                    insetAnimCurve: Curves.easeInOut,
                                    progress: 0.0,
                                    maxProgress: 100.0,
                                    progressTextStyle: TextStyle(
                                        color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
                                    messageTextStyle: TextStyle(
                                        color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
                                await pr.show();
                                model.registerAccount(context);
                                pr.hide();
                              },
                              child: Center(
                                child: Text(
                                  "Register",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ))
                      ],
                    ),
                  ),
                )));
      }
    });
  }
}
