import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/models/Actor.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:file_picker/file_picker.dart';
class CreateUserFilm extends StatelessWidget {







  @override
  Widget build(BuildContext context) {
    ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
    var  itemtempt = ["Chưa chọn"];
    return ScopedModelDescendant<CharacterViewModel>(
        builder: (context, child, model) {
          model.fetchListActor();
      return Scaffold(
          body: Padding(
              padding: EdgeInsets.only(top: 100.0, left: 25.0),
              child: Container(
                padding: new EdgeInsets.only(left: 5.0, right: 10.0, top: 50),
                width: ScreenUtil.getInstance().setWidth(950),
                height: ScreenUtil.getInstance().setHeight(1000),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          offset: Offset(0.0, 15.0),
                          blurRadius: 15.0),
                      BoxShadow(
                          color: Colors.black12,
                          offset: Offset(0.0, -10.0),
                          blurRadius: 10.0),
                    ]),
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Center(
                        child: Text(
                          "Thêm vai diễn",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ScreenUtil.getInstance().setSp(50)),
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.person),
                        title: DropdownButton<String>(
                          value: model.actorName != null ?  model.actorName : "Chưa chọn",
                          icon: Icon(Icons.arrow_drop_down),
                          iconSize: 24,
                          elevation: 16,
                          style: TextStyle(color: Colors.deepPurple),
                          underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                          ),
                          onChanged: (String newValue) {
                              model.changeText("actorId", newValue);
                              model.changeText("actorName", newValue);
                          },
                          items: model.currentListActor != null? model.currentListActor
                              .map<DropdownMenuItem<String>>((Actor value) {
                            return DropdownMenuItem<String>(
                              value: value.id,
                              child: Text(value.name),
                            );
                          }).toList() : itemtempt
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.person),
                        title: TextField(
                          decoration: InputDecoration(hintText: "Tên nhân vật"),
                          onChanged: (value) {
                            model.changeText("name", value);
                          },
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.note),
                        title: RaisedButton(onPressed: ()async {
                          File file = await FilePicker.getFile();
                          model.filmScript = file;
                        }, child: Text("Chọn kịch bản"),),
                      ),
                      model.filmScript != null ? ListTile(
                        leading: Icon(Icons.note),
                        title: Text("đã chọn"),
                      ) : Text(""),
                      SizedBox(
                        height: ScreenUtil.getInstance().setHeight(50),
                      ),
                      Center(
                          child: AnimatedContainer(
                        height: ScreenUtil.getInstance().setHeight(100),
                        width: ScreenUtil.getInstance().setWidth(400),
                        duration: Duration(seconds: 2),
                        curve: Curves.bounceIn,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(6.0),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFF6078ea).withOpacity(.3),
                                  offset: Offset(0.0, 8.0),
                                  blurRadius: 8.0)
                            ],
                          ),
                          child: InkWell(
                            onTap: () async{
                                  model.addActorToFilm(context);
                            },
                            child: Center(
                              child: Text(
                                "Thêm",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ))
                    ],
                  ),
                ),
              )));
    });
  }
}
