


import 'package:flutter/cupertino.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:journey/views/user_film/create/CreateUserFilm.dart';
import 'package:scoped_model/scoped_model.dart';

class ParentCreateUserFilm extends StatelessWidget{


  CharacterViewModel model;


  ParentCreateUserFilm(this.model);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<CharacterViewModel>(model: model, child: CreateUserFilm());
  }

}