



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:journey/views/user_film/create/ParentCreateUserFilm.dart';
import 'package:scoped_model/scoped_model.dart';

class UserFilmItem extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<CharacterViewModel>(builder: (context,child,model){
      ScreenUtil screenUtil = ScreenUtil.getInstance()..init(context);
      model.fetchCharacter();
      if(model.currentListCharacter != null){
        return new ListView.builder(
            itemCount: model.currentListCharacter.length,
            itemBuilder: (BuildContext ctxt, int index) => Card(
                elevation: 8.0,
                margin:
                new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                child: InkWell(
                  onTap: () => {
//                        Navigator.push(context, MaterialPageRoute(builder: (context) => ParentCreateUserFilm(model)))
                  },
                  child: Container(
                    decoration:
                    BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
                    child: ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        leading: Container(
                            padding: EdgeInsets.only(right: 12.0),
                            decoration: new BoxDecoration(
                                border: new Border(
                                    right: new BorderSide(
                                        width: 1.0, color: Colors.white24))),
                            child: Image.network(
                                model.currentListCharacter[index].avatar)),
                        title: Text(
                          model.currentListCharacter != null ?
                          model.currentListCharacter[index].actorName: "",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Column(
                          crossAxisAlignment:  CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                    "Vai diễn: ${model.currentListCharacter[index].charactorName}",
                                    style: TextStyle(color: Colors.white)),
                                Expanded(child: Container()),
//                                Text("${model.currentListTool[index]
//                                    .status}",
//                                    style: model.currentListTool[index].status == "Available" ? TextStyle(color: Colors.green, fontWeight: FontWeight.bold) :
//                                    model.currentListTool[index].status == "Deleted" ? TextStyle(color: Colors.red, fontWeight: FontWeight.bold) :
//                                    model.currentListTool[index].status == "Out of stock" ? TextStyle(color: Colors.orange, fontWeight: FontWeight.bold) : Text("")
//                                ),
                              ],
                            ),
                            SizedBox(height: ScreenUtil.getInstance().setHeight(10))
                          ],
                        ),
                        trailing: RaisedButton.icon(onPressed: (){
                          model.deleteCharacter(model.currentListCharacter[index].id, context);
                        }, icon: Icon(Icons.close, color: Colors.white,), label: Text("Xóa",style: TextStyle(color: Colors.white),), color: Colors.red,)),
                  ),
                )));
      }else{
        return Container();
      }
    });
  }
  
}