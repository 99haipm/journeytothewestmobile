

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';


class Message {
  String title,body;

  Message({this.title, this.body});
}


class NotificationManager {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<Message> messages = [];

  void unSubribe(String topic){
    _firebaseMessaging.unsubscribeFromTopic(topic);
  }

  void initFCM(BuildContext context, String actorID){
    _firebaseMessaging.getToken().then((value) => print(value));
    registerTopic(actorID);
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final notification = message['data'];
        messages.add(Message(
            title: notification['title'], body: notification['body']));
        await showDialog(context: context,
            builder: (BuildContext context){
              return AlertDialog(
                title: Text(messages[messages.length-1].title),
                content: Text(messages[messages.length-1].body),
              );
            }
        );
        if(messages.length > 0){
          messages.removeLast();
        }
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
  }

  void registerTopic(String actorID) {
    _firebaseMessaging.subscribeToTopic("MyTopic_"+actorID);
  }
}
