


import 'dart:io';
import 'dart:math';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/models/Actor.dart';
import 'package:journey/models/Charactor.dart';
import 'package:journey/models/Film.dart';
import 'package:journey/repos/ActorRepository.dart';
import 'package:journey/repos/FilmRepository.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class CharacterViewModel extends Model{
  List<Character> currentListCharacter;
  String filmId;
  String actorId,name;
  String actorName;
  File filmScript;
  String filePath;
  List<Actor> currentListActor;
  List<CharaterModel> characterOfActors = List();

  List<Film> currentListFilm = List();
  List<Film> currentTempListFilm = List();
  CharacterViewModel(this.filmId);
  DateTime begin,end;
  bool isFirstSchedual = true;

  void changeText(String type, String value){
    switch(type){
      case"filmId": filmId = value; break;
      case"actorId": actorId = value; break;
      case "name": name = value; break;
      case"actorName" : actorName = value; break;
    }
  }

  void fetchCharacterOfActor(){
    ActorRepository actorRepository = ActorRepositoryImp();
    actorRepository.getCharacterOfActor(actorId)
    .then((value){
      if(value != null){
        characterOfActors = value;
        notifyListeners();
      }
    });
  }

  void deleteCharacter(String id, BuildContext context) async{
    FilmRepository filmRepository = FilmRepositoryImpl();
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: 'Chờ giây lát.',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    await pr.show();
    filmRepository.deleteCharacterOfFilm(id).then((value) {
      if (value) {
        pr.hide();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SuccessDialog(
                  title: "Xóa thành công",
                  description: "",
                  buttonText: "Ok",
                  isLoginAgain: false,
                )));
      }
    });
  }

  void setTime(String type, DateTime value) {
    switch (type) {
      case "begin":
        begin = value;
        break;
      case "end":
        end = value;
        break;
    }
    notifyListeners();
  }

  void fetchCharacter(){
    FilmRepository filmRepository = FilmRepositoryImpl();
    filmRepository.fetchCharacters(filmId)
    .then((value){
      if(value != null){
        currentListCharacter = value;
        notifyListeners();
      }
    });
  }

  void fetchListActor(){
    ActorRepository actorRepository = ActorRepositoryImp();
    actorRepository.fetchActors().then((value){
      if(value != null){
        currentListActor = value;
        currentListActor.add(new Actor(id: "Chưa chọn",name: ""));
        notifyListeners();
      }
    });
  }

  Future<String> uploadFile(File file, String path) async {
    StorageReference storageReference =
    FirebaseStorage.instance.ref().child('scripts/${path}');
    StorageUploadTask uploadTask = storageReference.putFile(file);
    await uploadTask.onComplete;
    print('File Uploaded');
    String url = await storageReference.getDownloadURL();
    return url;
  }



  String _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));


  void addActorToFilm(BuildContext context) async{
    FilmRepository filmRepository = FilmRepositoryImpl();
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: 'Chờ giây lát.',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    await pr.show();
    if(filmScript != null && name != null && actorId != null){
      var length = filmScript.toString().split("/").length;
      String currentPathImage = filmScript.toString().split("/")[length - 1];
      String url = await uploadFile(filmScript, getRandomString(10)+"_"+currentPathImage);

      CreateCharacterModel model =  CreateCharacterModel(filmId, actorId, "", name, url);
      filmRepository.addActorToFilm(model).then((value){
        if(value){
//          AlertDialog(
//            title: new Text("Thêm đạo cụ"),
//            content: Text("Thêm thành công"),
//          );
          Navigator.of(context).pop();
          pr.hide();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SuccessDialog(
                    title: "Thêm thành công",
                    description: "",
                    buttonText: "Ok",
                    isLoginAgain: false,
                  )));
        }
      });
    }
  }

  void fetchFilmOfActor(){
    FilmRepository filmRepository = FilmRepositoryImpl();

    filmRepository.fetchFilmOfActorSchedual(actorId)
    .then((value){
      if(value != null){
        currentTempListFilm = value;
        if(isFirstSchedual){
          currentListFilm = currentTempListFilm;
          isFirstSchedual = false;
        }
        notifyListeners();
      }
    });
  }

  void clickToSearch(){
    if(begin != null && end != null){
      String currentBeginDay = begin.day < 10 ? "0"+begin.day.toString() : begin.day.toString();
      String currentBeginMonthDay = begin.month < 10 ? "0"+begin.month.toString() : begin.month.toString();
      String currentEndDay = end.day < 10 ? "0"+end.day.toString() : end.day.toString();
      String currentEndMonthDay = end.month < 10 ? "0"+end.month.toString() : end.month.toString();
      String currentBegin =currentBeginMonthDay + "/" +  currentBeginDay +"/" + begin.year.toString();
      String currentEnd =currentEndMonthDay + "/" + currentEndDay +"/" + end.year.toString();
      print(currentBegin);
      print(currentEnd);
      currentListFilm = currentTempListFilm.where((element) => element.begin == currentBegin && element.end == currentEnd).toList();
      notifyListeners();
    }
  }

}