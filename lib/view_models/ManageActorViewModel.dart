import 'package:journey/models/Actor.dart';
import 'package:journey/repos/ActorRepository.dart';
import 'package:scoped_model/scoped_model.dart';

class ManageActorViewModel extends Model {
  List<Actor> currentListActors = new List<Actor>();
  List<Actor> temptActor = new List<Actor>();
  bool firstTime = true;
  void fetchActors() async {
    ActorRepository actorRepository = new ActorRepositoryImp();
    actorRepository.fetchActors().then((value) {
      currentListActors.clear();
      value.forEach((element) {
        currentListActors.add(element);
        if(firstTime){
          temptActor = currentListActors;
          firstTime = false;
        }
        notifyListeners();
      });
    });
  }

  void liveSearch(String value) {
    temptActor = currentListActors.where((element) => element.name.toLowerCase().contains(value.toLowerCase())).toList();
    notifyListeners();
  }
}
