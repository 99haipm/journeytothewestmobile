



import 'package:flutter/material.dart';
import 'package:journey/models/User.dart';
import 'package:journey/repos/UserRepository.dart';
import 'package:scoped_model/scoped_model.dart';

class RegisterViewModel extends Model{

  String username,password,confirm;
  bool result = false;

  void changeText(String type, String value){
    switch(type){
      case "username":
        username = value;
        break;
      case "password":
        password = value;
        break;
      case "confirm":
        confirm = value;
        break;
    }
  }

  void registerAccount(BuildContext context){
    if(password != confirm){
      showDialog(context: context, builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text("Dialog"),
          content: Text("password must be match with confirm"),
        );
      });
    }else{
      UserRepository repo = UserRepositoryImp();
      UserRegisterModel model = UserRegisterModel(username, password, confirm);
      repo.registerAccount(model).then((value){
        if(value){
          result = true;
          notifyListeners();
        }
      });
    }
  }

}