


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/models/User.dart';
import 'package:journey/repos/UserRepository.dart';
import 'package:journey/view_models/HomeViewModel.dart';
import 'package:journey/views/home/HomeParent.dart';
import 'package:journey/views/home/home.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:progress_dialog/progress_dialog.dart';
class LoginViewModel extends Model{

  String username,password;

  void onChange(String target, String value){
      switch(target) {
        case "username":
          this.username = value;
          break;
        case "password":
          this.password = value;
          break;
      }
      notifyListeners();
  }

  void clickToLogin(BuildContext context) async{
    try{
      UserLoginModel model = UserLoginModel(this.username,this.password);
      UserRepository userRepository = UserRepositoryImp();
      ProgressDialog pr = ProgressDialog(context);
      pr.style(message: 'Chờ giây lát.',
          borderRadius: 10.0,
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
          insetAnimCurve: Curves.easeInOut,
          progress: 0.0,
          maxProgress: 100.0,
          progressTextStyle: TextStyle(
              color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
          messageTextStyle: TextStyle(
              color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
      await pr.show();
      if( await userRepository.login(model) != null){
        pr.hide();
        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeParent(model: HomeViewModel(),)));
      }
    }catch(Exception){
      showDialog(context: context,builder: (BuildContext context){
        return  AlertDialog(
          title: Text("thông báo"),
          content: Text("Đăng nhập không thành công"),
        );
      });
    }
  }


}