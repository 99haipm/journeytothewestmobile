import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:journey/models/Tool.dart';
import 'package:journey/models/ToolFilm.dart';
import 'package:journey/repos/FilmRepository.dart';
import 'package:journey/repos/ToolRepository.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class ToolViewModel extends Model {
  String name, desc, status;
  int quantity;
  File image;
  List<Tool> currentListTool;
  List<Tool> tempListTool;
  bool isFirstTime = true;
  Tool currentTool;
  String currentId;
  bool isChange = false;
  bool statusJob = false;
  String currentImage;
  DateTime begin, end;
  int maxQuantity;
  String filmId;
  String toolId;

  void ChangeText(String type, String value) {
    switch (type) {
      case "desc":
        desc = value;
        break;
      case "name":
        name = value;
        break;
      case "quantity":
        quantity = int.parse(value);
        break;
    }
  }

  TextEditingController quantityController2 = TextEditingController();

  void setTime(String type, DateTime value) {
    switch (type) {
      case "begin":
        begin = value;
        break;
      case "end":
        end = value;
        break;
    }
    quantityController2..text = this.quantity.toString();
    notifyListeners();
  }

  void PickImage() async {
    final pickedFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    image = new File(pickedFile.path);
    notifyListeners();
  }

  Future<String> uploadFile(File image, String path) async {
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child('tools/${path}');
    StorageUploadTask uploadTask = storageReference.putFile(image);
    await uploadTask.onComplete;
    print('File Uploaded');
    String url = await storageReference.getDownloadURL();
    return url;
  }

  void createTool(BuildContext context) async {
    if (name != null && desc != null && image != null && quantity != null) {

      ProgressDialog pr = ProgressDialog(context);
      pr.style(message: 'Chờ giây lát.',
          borderRadius: 10.0,
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
          insetAnimCurve: Curves.easeInOut,
          progress: 0.0,
          maxProgress: 100.0,
          progressTextStyle: TextStyle(
              color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
          messageTextStyle: TextStyle(
              color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
      await pr.show();
      var length = image.toString().split("/").length;
      String currentPathImage = image.toString().split("/")[length - 1];



      String url = await uploadFile(image, currentPathImage);

      ToolRepository toolRepository = ToolRepositoryImpl();
      ToolCreateModel model = ToolCreateModel(name, desc, url, quantity);
      toolRepository.create(model).then((value) {
        if (value) {
          pr.hide();
          Navigator.of(context).pop();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SuccessDialog(
                    title: "Tạo thành công",
                    description: "",
                    buttonText: "Ok",
                    isLoginAgain: false,
                  )));
        }
      });
    }
  }

  void fetchTool() {
    ToolRepository toolRepository = ToolRepositoryImpl();
    toolRepository.fetch().then((value) {
      if (currentListTool != null) {
        currentListTool.clear();
      }
      if (value.length >= 0) {
        currentListTool = value;
        if (isFirstTime) {
          tempListTool = currentListTool;
          isFirstTime = false;
        }
        notifyListeners();
      }
    });
  }

  void fetchToolById(String id) {
    ToolRepository toolRepository = ToolRepositoryImpl();
    toolRepository.getById(id).then((value) {
      if (value != null) {
        currentId = id;
        currentTool = value;
        notifyListeners();
      }
    });
  }

  void deleteTool(String id, BuildContext context) async{
    ToolRepository toolRepository = ToolRepositoryImpl();
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: 'Chờ giây lát.',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    await pr.show();
    toolRepository.delete(id).then((value) {
      notifyListeners();
      pr.hide();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SuccessDialog(
                    title: "Xóa thành công",
                    description: "",
                    buttonText: "Ok",
                    isLoginAgain: false,
                  )));
      return true;
    });
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  TextEditingController descController = TextEditingController();

  void initText() {
    if (currentTool != null) {
      nameController..text = currentTool.name;
      quantityController..text = currentTool.quantity.toString();
      descController..text = currentTool.description;
      currentImage = currentTool.image;
      notifyListeners();
      isChange = true;
    }
  }

  void updateTool(BuildContext context) async {
      ToolRepository toolRepository = ToolRepositoryImpl();
      String url;
      ProgressDialog pr = ProgressDialog(context);
      pr.style(message: 'Chờ giây lát.',
          borderRadius: 10.0,
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
          insetAnimCurve: Curves.easeInOut,
          progress: 0.0,
          maxProgress: 100.0,
          progressTextStyle: TextStyle(
              color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
          messageTextStyle: TextStyle(
              color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
      await pr.show();
      if(image != null){
        var length = image.toString().split("/").length;
        String currentPathImage = image.toString().split("/")[length - 1];
        url = await uploadFile(image, currentPathImage);
      }
      ToolCreateModel model = ToolCreateModel(name != null ? name : currentTool.name, desc != null ? desc : currentTool.description, url, quantity != null ? quantity : currentTool.quantity);
      toolRepository.update(model, currentTool.id).then((value) {
        if (value) {
          pr.hide();
          Navigator.of(context).pop();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SuccessDialog(
                        title: "Cập nhập thành công",
                        description: "",
                        buttonText: "Ok",
                        isLoginAgain: false,
                      )));
        }
      });
  }

  void addToolToFilm(String toolId, BuildContext context) {
    FilmRepository filmRepository = FilmRepositoryImpl();
    if (begin != null && end != null && quantity != null) {
      ToolFilmCartModel model = ToolFilmCartModel(toolId, begin, end, quantity);
      filmRepository.addToolToFilm(model, filmId).then((value) {
        if (value) {
          Navigator.of(context).pop();
          Navigator.of(context).pop();
          Navigator.of(context).pop();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SuccessDialog(
                        title: "Thêm thành công",
                        description: "",
                        buttonText: "Ok",
                        isLoginAgain: false,
                      )));
        }
      });
    }
  }

  void liveSearch(String value) {
    tempListTool = currentListTool
        .where((element) =>
            element.name.toLowerCase().contains(value.toLowerCase()))
        .toList();
    notifyListeners();
  }
}
