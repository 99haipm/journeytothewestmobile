

import 'package:journey/models/Actor.dart';
import 'package:journey/repos/ActorRepository.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DetailViewModel extends Model{

  Actor currentActor;
  String id;

  bool status = false;

  DetailViewModel({this.id});

  // ignore: non_constant_identifier_names
  void LoadActorDetail(){
    ActorRepository actorRepository = ActorRepositoryImp();
    actorRepository.GetActorById(this.id).then((value){
      currentActor = value;
      notifyListeners();
    });
  }

  // ignore: non_constant_identifier_names, missing_return
  Future<bool> DeleteActor(String id) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    if(token != null){
      ActorRepository actorRepository = ActorRepositoryImp();
      actorRepository.DeleteActor(id, token).then((value) {
        if(value){
          return true;
        }else{
          return false;
        }
      });
    }
  }


  // ignore: non_constant_identifier_names, missing_return
  Future<bool> ActiveActor(String id) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    if(token != null){
      ActorRepository actorRepository = ActorRepositoryImp();
      actorRepository.ActiveActor(id, token).then((value) {
        if(value){
          return true;
        }else{
          return false;
        }
      });
    }
  }
}