import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:journey/models/Actor.dart';
import 'package:journey/repos/ActorRepository.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';

class UpdateProfileViewModel extends Model {
  String id, name, phone, description, email;
  File avatar;
  bool status = false;

  //for updating
  Actor currentActor;


  void ChangeText(String type, String value) {
    switch (type) {
      case "id":
        id = value;
        break;
      case "name":
        name = value;
        break;
      case "phone":
        phone = value;
        break;
      case "description":
        description = value;
        break;
      case "email":
        email = value;
        break;
    }
    isEdited = true;
  }

  void PickImage() async{
    final pickedFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    avatar = new File(pickedFile.path);
    currentAvatar = null;
    notifyListeners();
  }


  Future<String> uploadFile(File image, String path) async {
    StorageReference storageReference =
    FirebaseStorage.instance.ref().child('images/${path}');
    StorageUploadTask uploadTask = storageReference.putFile(image);
    await uploadTask.onComplete;
    print('File Uploaded');
    String url = await storageReference.getDownloadURL();
    return url;
  }


  void UpdateProfile(String id,BuildContext context) async{
    if(description == null){
      description = "this is description";
    }
    if(avatar != null){
      ProgressDialog pr = ProgressDialog(context);
      pr.style(message: 'Chờ giây lát.',
          borderRadius: 10.0,
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
          insetAnimCurve: Curves.easeInOut,
          progress: 0.0,
          maxProgress: 100.0,
          progressTextStyle: TextStyle(
              color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
          messageTextStyle: TextStyle(
              color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
      await pr.show();
      var length = avatar.toString().split("/").length;
      String currentPathImage = avatar.toString().split("/")[length - 1];
      String url = await uploadFile(avatar, currentPathImage);
      ActorUpdateProfile model = ActorUpdateProfile(id, name, description, phone, email, url);
      ActorRepository actorRepository = ActorRepositoryImp();
      actorRepository.UpdateProfileFirstTime(model).then((value){
        if(value != null){
          status = true;
          pr.hide();
          notifyListeners();
        }
      });
    }
  }

  void updateActor(BuildContext context) async{
    if(description == null){
      description = "this is description";
    }
    String url;
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: 'Chờ giây lát.',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    await pr.show();
    if(avatar != null){
      var length = avatar.toString().split("/").length;
      String currentPathImage = avatar.toString().split("/")[length - 1];
      url = await uploadFile(avatar, currentPathImage);
    }
    ActorUpdate model = ActorUpdate(id, description, phone, email, url, name);
    ActorRepository actorRepository = ActorRepositoryImp();
    actorRepository.UpdateActor(model).then((value){
      if(value != null){
        pr.hide();
        status = true;
        notifyListeners();
      }
    });

  }

  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  String currentAvatar;
  bool isEdited = false;
  void fetchActor(){
    ActorRepository actorRepository = ActorRepositoryImp();
    actorRepository.GetActorById(id).then((value){
      if(value != null){
        currentActor = value;
        notifyListeners();
      }
    });
  }

  void initData(){
    if(currentActor != null && !isEdited){
      nameController..text = currentActor.name;
      phoneController..text = currentActor.phone;
      emailController..text = currentActor.email;
      currentAvatar =  currentActor.avatar;
      notifyListeners();
    }
  }

}
