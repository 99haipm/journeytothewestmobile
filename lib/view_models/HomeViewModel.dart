import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:journey/models/User.dart';
import 'package:journey/repos/UserRepository.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:journey/view_models/DetailViewModel.dart';
import 'package:journey/view_models/FilmViewModel.dart';
import 'package:journey/view_models/HisotryViewModel.dart';
import 'package:journey/view_models/ManageActorViewModel.dart';
import 'package:journey/view_models/NotificationManager.dart';
import 'package:journey/view_models/RegisterViewModel.dart';
import 'package:journey/view_models/ToolViewModel.dart';
import 'package:journey/view_models/UpdateProfileViewModel.dart';
import 'package:journey/views/character/ListCharacter.dart';
import 'package:journey/views/character/detail/ParentDetailActor.dart';
import 'package:journey/views/character/history/ParentHistory.dart';
import 'package:journey/views/character/schedual/ParentSchedual.dart';
import 'package:journey/views/film/create/ParentFilmCreate.dart';
import 'package:journey/views/film/home/ParentListFilm.dart';
import 'package:journey/views/history/ParentHistory.dart';
import 'package:journey/views/manage/MyListUser.dart';
import 'package:journey/views/manage/ParentDetailActor.dart';
import 'package:journey/views/register/UserRegisterParent.dart';
import 'package:journey/views/tool/create/ParentToolCreate.dart';
import 'package:journey/views/tool/home/ListTool.dart';
import 'package:journey/views/update/profile/UpdateProfileParent.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeViewModel extends Model {
  User currentUser;
  bool status = false;

  Items item1 =
      new Items(title: "Lịch Quay", img: "assets/calendar.png", role: "Actor", screen: "MyFilmOfActor");

  Items item2 =
      new Items(title: "Vai diễn", img: "assets/todo.png", role: "Actor",screen: "MyCharacters");
  Items item3 =
      new Items(title: "Hồ sơ", img: "assets/profile.png", role: "Actor", screen: "MyProfile");


  Items item5 = new Items(
      title: "Thêm diễn viên",
      img: "assets/add-user.png",
      role: "Admin",
      screen: "CreateUser");

  Items item6 = new Items(
      title: "Thêm phân cảnh",
      img: "assets/add-film.png",
      role: "Admin",
      screen: "MyCreateFilm");

  Items item7 = new Items(
      title: "Danh sách diễn viên",
      img: "assets/list-user.png",
      role: "Admin",
      screen: "MyListUser");

  Items item8 = new Items(
      title: "Lịch sử quay", img: "assets/history.png", role: "Actor", screen: "MyHistory");

  Items item9 = new Items(
      title: "Phân cảnh",
      img: "assets/video.png",
      role: "Admin",
      screen: "MyListFilm");

  Items item10 =
      new Items(title: "Đạo cụ", img: "assets/tool.png", role: "Admin", screen: "MyTool");
  Items item11 = new Items(
      title: "Thêm đạo cụ", img: "assets/add-tool.png", role: "Admin", screen: "MyAddTool");

  Items item12 = new Items(
      title: "Lịch sử thay đổi", img: "assets/history.png", role: "Actor", screen: "MyProfileHistory");

  List<Items> myList;

  HomeViewModel() {
    myList = [item1, item2, item3, item5, item6, item7, item8, item9, item10, item11, item12];
  }

  NotificationManager notificationManager = NotificationManager();
  void fetchCurrentUser(BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.get("token");
    UserRepository userRepository = UserRepositoryImp();
    if (token != null) {
      userRepository.fetchUser(token).then((value) {
        currentUser = value;
        if (!currentUser.modify) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => UpdateProfileParent(
                      UpdateProfileViewModel(), currentUser.id)));
        } else {
          myList = myList
              .where((element) => element.role.contains(currentUser.role))
              .toList();
          status = true;

          if(currentUser.role != "Admin"){
            notificationManager.initFCM(context,currentUser.id);
          }
          notifyListeners();
        }
      });
    } else {
      throw Exception("Fetch User Fail");
    }
  }

  void changeScreen(String screen, BuildContext context) {
    switch (screen) {
      case "MyListUser":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MyListUser(
                      model: ManageActorViewModel(),
                    )));
        break;
      case "CreateUser":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => UserRegisterParent(RegisterViewModel())));
        break;
      case "MyListFilm":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ParentListFilm(FilmViewModel())));
        break;

      case "MyCreateFilm":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ParentFilmCreate(FilmViewModel())));
        break;
      case "MyAddTool":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ParentToolCreate(ToolViewModel())));
        break;
      case "MyTool":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ListTool(ToolViewModel())));
        break;
      case "MyCharacters":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ListCharacter(CharacterViewModel(null),currentUser.id)));
        break;
      case "MyHistory":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ParentHistory(FilmViewModel(),currentUser.id)));
        break;
      case "MyProfile":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ParentDetailActorUpdate(UpdateProfileViewModel(),currentUser.id)));
        break;
      case "MyFilmOfActor":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ParentSchedual(CharacterViewModel(null),currentUser.id)));
        break;
      case "MyProfileHistory":
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ParentUserHistory(HistoryViewModel(),currentUser.id)));
        break;
    }
  }
}

class Items {
  String title;
  String subtitle;
  String event;
  String img;
  String role;
  String screen;

  Items(
      {this.title,
      this.subtitle,
      this.event,
      this.img,
      this.role,
      this.screen});
}
