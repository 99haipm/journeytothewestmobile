



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/models/ToolFilm.dart';
import 'package:journey/repos/FilmRepository.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:scoped_model/scoped_model.dart';

class ToolFilmViewModel extends Model{
  String currentFilmId;
  String currentFilmName;
  List<ToolFilm> currentListToolFilm;


  ToolFilmViewModel(this.currentFilmId, this.currentFilmName);

  void fetchToolFilm(){
    FilmRepository filmRepository = FilmRepositoryImpl();
    filmRepository.fetchToolOfFilm(currentFilmId).then((value){
      if(value != null){
        currentListToolFilm = value;
        notifyListeners();
      }
    });
  }

  void deleteTool(String id, BuildContext context) {
    FilmRepository filmRepository = FilmRepositoryImpl();
    filmRepository.deleteToolOfFilm(id).then((value) {
      if (value) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SuccessDialog(
                  title: "Xóa thành công",
                  description: "",
                  buttonText: "Ok",
                  isLoginAgain: false,
                )));
      }
    });
  }
}