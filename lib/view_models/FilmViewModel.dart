import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:journey/models/Film.dart';
import 'package:journey/repos/FilmRepository.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:intl/intl.dart';

class FilmViewModel extends Model {
  List<Film> listFilm = new List();
  List<Film> tempFilm = new List();
  Film currentFilm;
  String name, desc, location;
  DateTime begin, end;
  bool statusJob = false;
  bool isChange = false;
  bool isFirst = true;
  bool afterFinish = false;

  void changeText(String type, String value) {
    switch (type) {
      case "name":
        name = value;
        break;
      case "desc":
        desc = value;
        break;
      case "location":
        location = value;
        break;
    }
  }

  void setTime(String type, DateTime value) {
    print("updated");
    switch (type) {
      case "begin":
        begin = value;
        break;
      case "end":
        end = value;
        break;
    }
    print(begin);
    notifyListeners();
  }

  void fetchFilm() {
    FilmRepository filmRepository = FilmRepositoryImpl();
    filmRepository.fetchFilm().then((value) {
      listFilm.clear();
      if (value.length > 0) {
        listFilm = value;
        if (isFirst) {
          tempFilm = listFilm;
        }
      }
      notifyListeners();
    });
  }

  void getById(String id) {
    FilmRepository filmRepository = FilmRepositoryImpl();
    filmRepository.getById(id).then((value) {
      if (value != null) {
        currentFilm = value;
        notifyListeners();
      }
    });
  }

  void create(BuildContext context) async{
    FilmRepository filmRepository = FilmRepositoryImpl();

    FilmCreateModel model = FilmCreateModel(
        this.name, this.desc, this.location, this.begin, this.end);
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: 'Chờ giây lát.',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    await pr.show();
    filmRepository.createFilm(model).then((value) {
      if (value) {
        statusJob = true;
        pr.hide();
        notifyListeners();
      }
    });
  }

  // ignore: missing_return
  void delete(String id, BuildContext context) async{
    FilmRepository filmRepository = FilmRepositoryImpl();
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: 'Chờ giây lát.',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    await pr.show();
    filmRepository.deleteFilm(id).then((value) {
      if (value) {
        pr.hide();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SuccessDialog(
                      title: "Xóa thành công",
                      description: "",
                      buttonText: "Ok",
                      isLoginAgain: false,
                    )));
      }
    });
  }
  
  TextEditingController nameController = TextEditingController();
  TextEditingController descController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  void initText() {
    if (currentFilm != null) {
      nameController..text = currentFilm.name;
      descController..text = currentFilm.desc;
      locationController..text = currentFilm.location;
      DateFormat format = DateFormat("MM/dd/yyyy");
      begin = format.parse(currentFilm.begin);
      end = format.parse(currentFilm.end);
      notifyListeners();
      isChange = true;
    }
  }

  void update(BuildContext context) {
    FilmRepository filmRepository = FilmRepositoryImpl();
    FilmUpdateModel model = FilmUpdateModel(
        id: currentFilm.id,
        name: this.name != null ? this.name : currentFilm.name,
        location: this.location != null ? this.location : currentFilm.location,
        desc: this.desc != null ? this.desc : currentFilm.desc,
        begin: this.begin,
        end: this.end);
    print(model);
    filmRepository.updateFilm(model).then((value) {
      if (value) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SuccessDialog(
                      title: "Cập nhật thành công",
                      description: "",
                      buttonText: "Ok",
                      isLoginAgain: false,
                    )));
      }
    });
  }

  void liveSearch(String value) {
    tempFilm = listFilm
        .where((element) =>
            element.name.toLowerCase().contains(value.toLowerCase()))
        .toList();
    notifyListeners();
  }

  void finishFilm(String filmID, BuildContext context) async{
    FilmRepository filmRepository = FilmRepositoryImpl();
    ProgressDialog pr = ProgressDialog(context);
    pr.style(message: 'Chờ giây lát.',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    await pr.show();
    filmRepository.finishFilm(filmID).then((value) {
      if (value) {
        notifyListeners();
        pr.hide();
        Navigator.of(context).pop();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SuccessDialog(
                      title: "Cập nhật thành công",
                      description: "",
                      buttonText: "Ok",
                      isLoginAgain: false,
                    )));
        afterFinish = true;
      }
    });
  }

  void fetchFinishFilm(String actorId) {
    FilmRepository filmRepository = FilmRepositoryImpl();
    filmRepository.fetchFinishFilmOfActor(actorId).then((value) {
      listFilm.clear();
      if (value.length > 0) {
        listFilm = value;
        if (isFirst) {
          tempFilm = listFilm;
        }
      }
      notifyListeners();
    });
  }
}
