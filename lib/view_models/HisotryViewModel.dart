import 'package:journey/models/UserHistory.dart';
import 'package:journey/repos/UserRepository.dart';
import 'package:scoped_model/scoped_model.dart';

class HistoryViewModel extends Model {

  String currentUserId;
  List<UserHistory> currentListHistory = new List();


  void fetchHistory() async{
    UserRepository userRepository = UserRepositoryImp();

    await userRepository.getHistory(currentUserId).then((value){

      if(value != null){
        currentListHistory = value;
        notifyListeners();
      }
    });
  }
}