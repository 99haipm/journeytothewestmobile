


class ToolFilm{
  String toolId,toolName;
  String borrowFrom,borrowTo;
  int quantity;
  String image;
  String id;


  ToolFilm(this.toolId, this.toolName, this.borrowFrom, this.borrowTo,
      this.quantity, this.image, this.id);

  factory ToolFilm.fromJson(Map<String,dynamic> json) {
    return ToolFilm(json["tool-id"], json["tool-name"], json["borrow-from"].toString().split(" ")[0], json["borrow-to"].toString().split(" ")[0], json["quantity"], json["tool-image"],json["tool-film-id"]);
  }
}


class ToolFilmCartModel{
  String toolId;
  DateTime from,to;
  int quantity;

  ToolFilmCartModel(this.toolId, this.from, this.to, this.quantity);
}