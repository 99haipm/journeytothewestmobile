




import 'package:flutter/foundation.dart';

class UserHistory {
  String desc,by,date;

  UserHistory(this.desc, this.by, this.date);

  factory UserHistory.fromJson(Map<String,dynamic> json){
    return UserHistory(
      json["description"],json["modify-by"],json["date-modify"]
    );
  }
}