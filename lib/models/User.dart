


import 'package:flutter/foundation.dart';

class User{
  String id;
  String username;
  String fullname;
  String role;
  String avatar;
  bool status;
  bool modify;
  User({this.id,this.username,this.fullname,this.role,this.avatar,this.status,this.modify});


  factory User.fromJson(Map<String, dynamic> json){
     return User(
       id: json["id"],
       username: json["username"],
       fullname: json["fullname"],
       role: json["role"],
       avatar: json["avatar"],
       status: json["status"],
       modify: json["updated"]
     );
  }
}

class UserLoginModel{
  String username;
  String password;

  UserLoginModel(String username, String password){
    this.username = username;
    this.password = password;
  }
}

class UserRegisterModel{
  String username;
  String password;
  String confirm;
  UserRegisterModel(this.username, this.password, this.confirm);
}