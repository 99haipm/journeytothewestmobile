
import 'dart:io';

class Actor{
  String id,name,decsription,avatar,phone,email;
  bool status;
  Actor({this.id,this.name,this.decsription,this.avatar,this.phone,this.email,this.status});

  factory Actor.fromJson(Map<String, dynamic> json){
    return Actor(
      id: json["id"],
      name: json["name"],
      decsription: json["description"],
      avatar: json["avatar"],
      phone: json["phone"],
      email: json["email"],
      status: json["status"]
    );
  }
}

class ActorUpdateProfile{
  String id,name,decsription,phone,email;
  String avatar;

  ActorUpdateProfile(this.id, this.name, this.decsription, this.phone,
      this.email, this.avatar);
}


class ActorUpdate{
  String id,description,phone,email,name;
  String avatar;

  ActorUpdate(this.id, this.description, this.phone, this.email, this.avatar,this.name);
}