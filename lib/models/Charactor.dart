


class Character{
  String filmId,actorId,description,charactorName,filmScript,actorName,filmName,avatar;
  String id;
  Character(this.filmId, this.actorId, this.description, this.charactorName,
      this.filmScript, this.actorName, this.filmName,this.avatar, this.id);
  
  
  factory Character.fromJson(Map<String,dynamic> json){
    return Character(json["film-id"], json["actor-id"], json["description"], json["charactor-name"], json["film-script"], json["actor-name"], json["film-name"],json["actor-image"],json["character-id"]);
  }
}

class CreateCharacterModel{
  String filmId,actorId,description,name,filmScript;

  CreateCharacterModel(
      this.filmId, this.actorId, this.description, this.name, this.filmScript);
}


class CharaterModel {
  String filmId,filmName,characterName,script;

  CharaterModel(this.filmId, this.filmName, this.characterName, this.script);

  factory CharaterModel.fromJson(Map<String,dynamic> json){
    return CharaterModel(json["film-id"], json["film-name"], json["character-name"], json["film-script"]);
  }
}