


import 'package:flutter/foundation.dart';
import 'package:journey/view_models/HomeViewModel.dart';

class Film{
  String id,name,desc,location;
  String begin,end;
  String status;

  Film({this.id, this.name, this.desc, this.location, this.begin, this.end,
      this.status});

  factory Film.fromJson(Map<String, dynamic>json){
    return Film(
      id : json["id"],
      name: json["name"],
      begin:  json["begin-time"].toString().split(" ")[0],
      end : json["end-time"].toString().split(" ")[0],
      desc: json["description"],
      status: json["status"],
      location: json["location"]
    );
  }
}

class FilmCreateModel{
  String name,desc,location;
  DateTime begin,end;

  FilmCreateModel(this.name, this.desc, this.location, this.begin, this.end);
}


class FilmUpdateModel {
  String id, name, desc, location;
  DateTime begin, end;
  String status;

  FilmUpdateModel(
      {this.id, this.name, this.desc, this.location, this.begin, this.end,
        this.status});

  factory FilmUpdateModel.fromJson(Map<String, dynamic>json){
    return FilmUpdateModel(
        id: json["id"],
        name: json["name"],
        begin: json["begin-time"],
        end: json["end-time"],
        desc: json["description"],
        status: json["status"],
        location: json["location"]
    );
  }
}
