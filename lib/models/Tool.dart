import 'dart:io';

class Tool {
  String id, name, description, image, status;
  int quantity;

  Tool(this.id, this.name, this.description, this.image, this.status,
      this.quantity);

  factory Tool.fromJson(Map<String, dynamic> json) {
    return Tool(json["id"], json["name"], json["description"], json["image"],
        json["status"], json["quantity"]);
  }
}

class ToolCreateModel{
  String name,description;
  int quantity;
  String image;

  ToolCreateModel(this.name, this.description, this.image, this.quantity);
}
