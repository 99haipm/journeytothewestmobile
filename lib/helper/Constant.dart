



class RootApi{
  static const String API_ROOT = "https://journeytowest.azurewebsites.net/";
}


class UserAPI{
  static const String LOGIN = RootApi.API_ROOT+"api/user/login";
  static const String FETCH_USER = RootApi.API_ROOT+"api/user";
  static const String REGISTER_ACCOUNT = RootApi.API_ROOT+"api/user/register-account";
  static const String GET_HISTORY = RootApi.API_ROOT+"api/user/get-histories?userId=";
}

class ActorAPI{
  static const String FETCH_ACTOR = RootApi.API_ROOT+"api/actor?limit=-1";
  static const String GET_DETAIL_ACTOR = RootApi.API_ROOT+"api/actor/get-by-id?id=";
  static const String UPDATE_PROFILE_ACTOR = RootApi.API_ROOT+"api/actor";
  static const String DELETE_ACTOR = RootApi.API_ROOT+"api/actor?id=";
  static const String ACTIVE_ACTOR = RootApi.API_ROOT+"api/actor/active?id=";
  static const String UPDATE_ACTOR = RootApi.API_ROOT+"api/actor?id=";
}

class FilmAPI{
  static const String FETCH_FILM = RootApi.API_ROOT + "api/films";
  static const String GET_DETAIL_FILM = RootApi.API_ROOT + "api/films/get-by-id?id=";
  static const String CREATE_FILM = RootApi.API_ROOT + "api/films";
  static const String DELETE_FILM = RootApi.API_ROOT + "api/films?filmID=";
  static const String EDIT_FILM = RootApi.API_ROOT + "api/films?filmID=";
  static const String FETCH_TOOL_FILM = RootApi.API_ROOT + "api/films/get-tool-of-film?filmId=";
  static const String ADD_TOOL_FILM = RootApi.API_ROOT + "api/films/add-tool-film";
  static const String GET_ACTOR_OF_FILM = RootApi.API_ROOT+ "api/films/get-actor-of-film?filmId=";
  static const String CREATE_CHARACTER = RootApi.API_ROOT + "api/films/add-actor-to-film";
  static const String GET_CHARACTER_OF_ACTOR = RootApi.API_ROOT + "api/films/get-character-of-actor?actorId=";
  static const String FINISH_FILM = RootApi.API_ROOT + "api/films/finish-film?filmID=";
  static const String GET_FINISH_FILM_ACTOR = RootApi.API_ROOT + "api/films/get-finish-film-actor?actorId=";
  static const String GET_SCHEDUAL_FILM_ACTOR = RootApi.API_ROOT+"api/films/get-film-of-actor?actorId=";
  static const String DELETE_CHARACTER_OF_FILM = RootApi.API_ROOT +"api/films/delete-character?characterId=";
  static const String DELETE_TOOL_OF_FILM = RootApi.API_ROOT +"api/films/delete-tool-of-film?id=";

}

class ToolAPI{
  static const String ADD_TOOL = RootApi.API_ROOT + "api/tools";
  static const String FETCH_TOOL = RootApi.API_ROOT + "api/tools";
  static const String GET_BY_ID = RootApi.API_ROOT + "api/tools/get-by-id?id=";
  static const String DELETE_TOOL = RootApi.API_ROOT + "api/tools?id=";
  static const String UPDATE_TOOL = RootApi.API_ROOT + "api/tools?id=";
}