import 'dart:convert';
import 'dart:math';

import 'package:journey/helper/Constant.dart';
import 'package:journey/models/Tool.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

abstract class ToolRepository {
  Future<bool> create(ToolCreateModel model);

  Future<List<Tool>> fetch();

  Future<Tool> getById(String id);

  Future<bool> delete(String id);

  Future<bool> update(ToolCreateModel model, String id);
}

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

class ToolRepositoryImpl with ToolRepository {
  @override
  Future<bool> create(ToolCreateModel model) async {
    final http.Response response = await http.post(ToolAPI.ADD_TOOL,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, String>{
          "name": model.name,
          "quantity": model.quantity.toString(),
          "description": model.description,
          "image": model.image
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<List<Tool>> fetch() async {
    var response = await http.get(ToolAPI.FETCH_TOOL, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if (response.statusCode == 200) {
      List<dynamic> temp = json.decode(response.body);
      List<Tool> listTool = List();
      for (var elm in temp) {
        var map = elm as Map<String, dynamic>;
        listTool.add(Tool.fromJson(map));
      }
      return listTool;
    } else {
      throw Exception("Status code is : " + response.statusCode.toString());
    }
  }

  @override
  Future<Tool> getById(String id) async {
    if (id != null) {
      String url = ToolAPI.GET_BY_ID + id;
      var response = await http.get(url, headers: <String, String>{
        "accept": "application/json",
        "Content-Type": "application/json; charset=UTF-8"
      });
      if (response.statusCode == 200) {
        return Tool.fromJson(json.decode(response.body));
      } else {
        throw Exception("Status code is : " + response.statusCode.toString());
      }
    }
  }

  @override
  Future<bool> delete(String id) async {
    var response = await http.delete(ToolAPI.DELETE_TOOL + id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is : " + response.statusCode.toString());
    }
  }

  @override
  Future<bool> update(ToolCreateModel model, String id) async {
    final http.Response response = await http.put(ToolAPI.UPDATE_TOOL+id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, String>{
          "name": model.name,
          "quantity": model.quantity.toString(),
          "description": model.description,
          "image": model.image
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }
}
