import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:journey/helper/Constant.dart';
import 'package:journey/models/Actor.dart';
import 'package:http/http.dart' as http;
import 'package:journey/models/Charactor.dart';
import 'package:journey/models/Film.dart';
import 'package:journey/view_models/CharacterViewModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ActorRepository {
  Future<List<Actor>> fetchActors();

  // ignore: non_constant_identifier_names
  Future<Actor> GetActorById(String id);
  Future<String> UpdateProfileFirstTime(ActorUpdateProfile model);
  Future<bool> DeleteActor(String id, String token);
  Future<bool> ActiveActor(String id, String token);
  Future<List<CharaterModel>> getCharacterOfActor (String actorId);
  Future<String> UpdateActor(ActorUpdate model);
}


class ActorRepositoryImp with ActorRepository {
  @override
  Future<List<Actor>> fetchActors() async {
    final response = await http.get(
        ActorAPI.FETCH_ACTOR, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if (response.statusCode == 200) {
      List<dynamic> listTemp = json.decode(response.body);
      List<Actor> listActor = new List<Actor>();
      if (listTemp != null) {
        for (var elm in listTemp) {
          var map = elm as Map<String, dynamic>;
          listActor.add(Actor.fromJson(map));
        }
        return listActor;
      }
      else {
        throw Exception("Error listTemp null");
      }
    }
    else {
      throw Exception("Error when call api fetch Actor");
    }
  }

  @override
  // ignore: non_constant_identifier_names
  Future<Actor> GetActorById(String id) async {
    var response = await http.get(
        ActorAPI.GET_DETAIL_ACTOR + id, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if(response.statusCode == 200){
      return Actor.fromJson(json.decode(response.body));
    }
    else{
      throw Exception("Actor is null");
    }
  }

  @override
  Future<String> UpdateProfileFirstTime(ActorUpdateProfile model) async{
    final http.Response response = await http.post(ActorAPI.UPDATE_PROFILE_ACTOR,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, String>{
          "id": model.id,
          "name": model.name,
          "description": model.decsription,
          "phone": model.phone,
          "email": model.email,
          "avatar": model.avatar
        }));
    if(response.statusCode == 200){
      return model.id;
    }else{
      throw Exception("Status code is : "+ response.statusCode.toString());
    }
  }

  @override
  Future<bool> DeleteActor(String id, String token) async {
    final http.Response response =
        await http.delete(ActorAPI.DELETE_ACTOR+id, headers: <String, String>{
      'accept': 'application/json',
      "Content-Type": "application/json; charset=UTF-8",
      "Authorization": "Bearer " + token
    });
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Invalid Token");
    }
  }

  @override
  Future<bool> ActiveActor(String id, String token) async{
    final http.Response response =
        await http.get(ActorAPI.ACTIVE_ACTOR+id, headers: <String, String>{
      'accept': 'application/json',
      "Content-Type": "application/json; charset=UTF-8",
      "Authorization": "Bearer " + token
    });
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Invalid Token");
    }
  }

  @override
  Future<List<CharaterModel>> getCharacterOfActor(String actorId) async{
    final response = await http.get(FilmAPI.GET_CHARACTER_OF_ACTOR+actorId,headers: <String, String>{
      'accept': 'application/json',
      "Content-Type": "application/json; charset=UTF-8",
    });
    if(response.statusCode == 200){
      List<dynamic> tempt = List();
      List<CharaterModel> currentList = List<CharaterModel>();
      for(var elm in json.decode(response.body)){
        var map = elm as Map<String,dynamic>;
        currentList.add(CharaterModel.fromJson(map));
      }
      return currentList;
    }else{
      throw Exception("status code is:" + response.statusCode.toString());
    }
  }

  @override
  Future<String> UpdateActor(ActorUpdate model) async {
    final http.Response response = await http.put(ActorAPI.UPDATE_ACTOR+model.id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, String>{
          "name": model.name,
          "description": model.description,
          "phone": model.phone,
          "email": model.email,
          "avatar": model.avatar
        }));
    if(response.statusCode == 200){
      return model.id;
    }else{
      throw Exception("Status code is : "+ response.statusCode.toString());
    }
  }



}