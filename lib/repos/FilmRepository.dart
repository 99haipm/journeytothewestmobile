import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:journey/helper/Constant.dart';
import 'package:journey/models/Charactor.dart';

import 'package:journey/models/Film.dart';
import 'package:journey/models/ToolFilm.dart';
import 'package:journey/views/tool/home/ListTool.dart';

abstract class FilmRepository {
  Future<List<Film>> fetchFilm();

  Future<Film> getById(String id);

  Future<bool> createFilm(FilmCreateModel model);

  Future<bool> deleteFilm(String id);

  Future<bool> updateFilm(FilmUpdateModel model);

  Future<List<ToolFilm>> fetchToolOfFilm(String filmId);

  Future<bool> addToolToFilm(ToolFilmCartModel model, String filmId);

  Future<List<Character>> fetchCharacters(String filmId);

  Future<bool> addActorToFilm(CreateCharacterModel model);
  Future<bool> finishFilm(String filmId);
  Future<List<Film>> fetchFinishFilmOfActor(String actorId);
  Future<List<Film>> fetchFilmOfActorSchedual(String id);
  Future<bool> deleteCharacterOfFilm(String characterId);
  Future<bool> deleteToolOfFilm(String id);
}

const String _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));


class FilmRepositoryImpl with FilmRepository {
  @override
  Future<List<Film>> fetchFilm() async {
    var response = await http.get(FilmAPI.FETCH_FILM, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if (response.statusCode == 200) {
      List<dynamic> listTempt = json.decode(response.body);
      List<Film> result = new List<Film>();
      if (listTempt != null) {
        for (var tempt in listTempt) {
          var map = tempt as Map<String, dynamic>;
          result.add(Film.fromJson(map));
        }
        return result;
      } else {
        throw Exception("Fetch Film Error");
      }
    } else {
      throw Exception("Fetch Status is" + response.statusCode.toString());
    }
  }

  @override
  Future<Film> getById(String id) async {
    var response = await http.get(FilmAPI.GET_DETAIL_FILM + id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      return Film.fromJson(json.decode(response.body));
    } else {
      throw Exception("Status code is : " + response.statusCode.toString());
    }
  }

  @override
  Future<bool> createFilm(FilmCreateModel model) async {
    var response = await http.post(FilmAPI.CREATE_FILM,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, dynamic>{
          "name": model.name.toString(),
          "description": model.desc.toString(),
          "location": model.location.toString(),
          "begin-time": model.begin.toIso8601String(),
          "end-time": model.end.toIso8601String(),
        }));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<bool> deleteFilm(String id) async {
    var response = await http.delete(FilmAPI.DELETE_FILM + id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is :" + response.statusCode.toString());
    }
  }

  @override
  Future<bool> updateFilm(FilmUpdateModel model) async {
    var response = await http.put(FilmAPI.EDIT_FILM + model.id,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, dynamic>{
          "name": model.name.toString(),
          "description": model.desc.toString(),
          "location": model.location.toString(),
          "begin-time": model.begin.toIso8601String(),
          "end-time": model.end.toIso8601String(),
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<List<ToolFilm>> fetchToolOfFilm(String filmId) async {
    var response = await http.get(FilmAPI.FETCH_TOOL_FILM + filmId,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      List<dynamic> listTempt = json.decode(response.body);
      List<ToolFilm> listToolFilm = new List<ToolFilm>();
      if (listTempt != null) {
        for (var item in listTempt) {
          var map = item as Map<String, dynamic>;
          listToolFilm.add(ToolFilm.fromJson(map));
        }
      }
      return listToolFilm;
    }
  }

  @override
  Future<bool> addToolToFilm(ToolFilmCartModel model, String filmId) async {
    var response = await http.post(FilmAPI.ADD_TOOL_FILM,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, dynamic>{
          "film-id": filmId,
          "tools": [
            {
              "tool-id": model.toolId,
              "borrow-from": model.from.toIso8601String(),
              "borrow-to": model.to.toIso8601String(),
              "quantity": model.quantity
            }
          ]
        }));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<List<Character>> fetchCharacters(String filmId) async {
    var response = await http.get(FilmAPI.GET_ACTOR_OF_FILM + filmId,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        });
    if (response.statusCode == 200) {
      List<dynamic> tempt = json.decode(response.body);
      List<Character> listCharacter = List();
      for (var elm in tempt) {
        var map = elm as Map<String, dynamic>;
        listCharacter.add(Character.fromJson(map));
      }
      return listCharacter;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<bool> addActorToFilm(CreateCharacterModel model) async {
    final http.Response response = await http.post(FilmAPI.CREATE_CHARACTER,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, String>{
          "film-id": model.filmId,
          "actor-id": model.actorId,
          "description": model.description,
          "name": model.name,
          "film-script": model.filmScript
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<bool> finishFilm(String filmId) async {
    var response = await http.get(FilmAPI.FINISH_FILM+filmId,headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if(response.statusCode == 200){
      return true;
    }
    else{
      throw Exception("Status code is: " + response.statusCode.toString());
    }
  }

  @override
  Future<List<Film>> fetchFinishFilmOfActor(String actorId) async {
    var response = await http.get(FilmAPI.GET_FINISH_FILM_ACTOR+actorId, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if (response.statusCode == 200) {
      List<dynamic> listTempt = json.decode(response.body);
      List<Film> result = new List<Film>();
      if (listTempt != null) {
        for (var tempt in listTempt) {
          var map = tempt as Map<String, dynamic>;
          result.add(Film.fromJson(map));
        }
        return result;
      } else {
        throw Exception("Fetch Film Error");
      }
    } else {
      throw Exception("Fetch Status is" + response.statusCode.toString());
    }
  }

  @override
  Future<List<Film>> fetchFilmOfActorSchedual(String id) async {
    var response = await http.get(FilmAPI.GET_SCHEDUAL_FILM_ACTOR+id, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if (response.statusCode == 200) {
      List<dynamic> listTempt = json.decode(response.body);
      List<Film> result = new List<Film>();
      if (listTempt != null) {
        for (var tempt in listTempt) {
          var map = tempt as Map<String, dynamic>;
          result.add(Film.fromJson(map));
        }
        return result;
      } else {
        throw Exception("Fetch Film Error");
      }
    } else {
      throw Exception("Fetch Status is" + response.statusCode.toString());
    }
  }

  @override
  Future<bool> deleteCharacterOfFilm(String characterId) async{
    var response = await http.delete(FilmAPI.DELETE_CHARACTER_OF_FILM+characterId, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if(response.statusCode == 200){
      return true;
    }else{
      throw Exception("Status code is :" + response.statusCode.toString());
    }
  }

  @override
  Future<bool> deleteToolOfFilm(String id) async{
    var response = await http.delete(FilmAPI.DELETE_TOOL_OF_FILM+id, headers: <String, String>{
      "accept": "application/json",
      "Content-Type": "application/json; charset=UTF-8"
    });
    if(response.statusCode == 200){
      return true;
    }else{
      throw Exception("Status code is :" + response.statusCode.toString());
    }
  }
}
