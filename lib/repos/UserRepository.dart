import 'dart:convert';

import 'package:journey/helper/Constant.dart';
import 'package:journey/models/User.dart';
import 'package:http/http.dart' as http;
import 'package:journey/models/UserHistory.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserRepository {
  Future<User> fetchUser(String token);

  Future<String> login(UserLoginModel model);

  Future<bool> registerAccount(UserRegisterModel model);

  Future<List<UserHistory>> getHistory(String id);
}

class UserRepositoryImp with UserRepository {
  http.Client client = new http.Client();

  @override
  Future<User> fetchUser(String token) async {
    final http.Response response =
        await client.get(UserAPI.FETCH_USER, headers: <String, String>{
      'accept': 'application/json',
      "Content-Type": "application/json; charset=UTF-8",
      "Authorization": "Bearer " + token
    });
    if (response.statusCode == 200) {
      return User.fromJson(json.decode(response.body));
    } else {
      throw Exception("Invalid Token");
    }
  }

  @override
  Future<String> login(UserLoginModel model) async {
    final http.Response response = await client.post(UserAPI.LOGIN,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, String>{
          "username": model.username,
          "password": model.password
        }));
    print(json.encode(<String, String>{
      "username": model.username,
      "password": model.password
    }));
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.remove("token");
      sharedPreferences.setString("token", json.decode(response.body));
      return response.body;
    } else {
      throw Exception("Username or Password invalid");
    }
  }

  @override
  Future<bool> registerAccount(UserRegisterModel model) async {
    // TODO: implement registerAccount
    final http.Response response = await http.post(UserAPI.REGISTER_ACCOUNT,
        headers: <String, String>{
          "accept": "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: json.encode(<String, String>{
          "username": model.username,
          "password": model.password,
          "confirm_password": model.confirm
        }));

    if (response.statusCode == 200) {
      print("SUCCESS");
      return true;
    } else {
      print("Fail");
      return false;
    }
  }

  @override
  Future<List<UserHistory>> getHistory(String id) async {
    final response = await http.get(UserAPI.GET_HISTORY+id,headers: <String, String>{
      'accept': 'application/json',
      "Content-Type": "application/json; charset=UTF-8",
    });
    if(response.statusCode == 200){
      List<UserHistory> currentList = List<UserHistory>();
      for(var elm in json.decode(response.body)){
        var map = elm as Map<String,dynamic>;
        currentList.add(UserHistory.fromJson(map));
      }
      return currentList;
    }else{
      throw Exception("status code is:" + response.statusCode.toString());
    }
  }
}
