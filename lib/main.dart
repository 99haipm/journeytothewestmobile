import 'package:flutter/material.dart';
import 'package:journey/view_models/HomeViewModel.dart';
import 'package:journey/view_models/LoginViewModel.dart';
import 'package:journey/view_models/ManageActorViewModel.dart';
import 'package:journey/view_models/UpdateProfileViewModel.dart';
import 'package:journey/views/dialog/SuccessDialog.dart';
import 'package:journey/views/home/home.dart';
import 'package:journey/views/manage/DetailActor.dart';
import 'package:journey/views/manage/MyListUser.dart';
import 'package:journey/views/manage/UserItems.dart';
import 'package:journey/views/register/UsernameRegister.dart';
import 'package:journey/views/update/profile/UpdateProfile.dart';
import 'package:journey/views/update/profile/UpdateProfileParent.dart';
import 'views/login/login.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Journey To The West",
      home: LoginPage(LoginViewModel())
//        home: UpdateProfileParent(UpdateProfileViewModel())
    );
  }

}
